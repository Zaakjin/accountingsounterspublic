﻿using System.ComponentModel.DataAnnotations;

namespace AccountingСounters.Enums
{
    public enum CounterType
    {
        [Display(Name = "ТЭ",           Description = "тепловой энергии")]
        HeatEnergyNode,
        [Display(Name = "Изм. модуль",  Description = "Измерительный модуль")]
        HeatEnergyNodeMeasuringModule,
        [Display(Name = "Термосопр.",   Description = "Термосопротивление")]
        HeatEnergyNodeThermoResistance,

        [Display(Name = "ХВС",          Description = "холодного водоснабжения")]
        ColdWaterNode,

        [Display(Name = "ГВС",          Description = "горячего водоснабжения")]
        HotWaterNode,

        [Display(Name = "ЭЭ",           Description = "электроэнергии")]
        ElectricNode,

        [Display(Name = "Трансф. тока", Description = "Трансформатор тока")]
        CurrentTransformer
    }

    public enum CounterSubType
    {
        [Display(Name = "", Description = "")]
        Default,
        [Display(Name = "(М Разб.)",  Description = "механический (разбор)")]
        Mechanical,
        [Display(Name = "(М Цирк.)",  Description = "механический (циркуляция)")]
        MechanicalСirc,
        [Display(Name = "(Э  Разб.)", Description = "электронный (разбор)")]
        Digital,
        [Display(Name = "(Э Цирк.)",  Description = "электронный (циркуляция)")]
        DigitalСirc,
        [Display(Name = "подача", Description = "подача")]
        Suply,
        [Display(Name = "обратка", Description = "обратка")]
        Return
    }
}