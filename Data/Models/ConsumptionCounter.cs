﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using AccountingСounters.Database;
using AccountingСounters.Enums;
using Data.Enums;

namespace AccountingСounters.Models
{
    public class ConsumptionCounter
    {
        public ConsumptionCounter() { }
        //public ConsumptionCounter(CounterType type, CounterSubType subType, string extraType, string model, string number, DateTime nextCheckDate, int maintainedFacilityId, int? associatedDeviceID = null)
        //{
        //    Type = type;
        //    SubType = subType;
        //    ExtraType = extraType;
        //    Model = model;
        //    Number = number;
        //    NextCheckDate = nextCheckDate;
        //    MaintainedFacilityID = maintainedFacilityId;
        //    AssociatedDeviceID = associatedDeviceID;
        //    Indications = new List<Indication>();
        //}

        public ConsumptionCounter(CounterType type, CounterSubType subType, string extraType, string model, string number, DateTime nextCheckDate, int maintainedFacilityId, ConsumptionCounter associatedDevice = null)
        {
            Type = type;
            SubType = subType;
            ExtraType = extraType;
            Model = model;
            Number = number;
            NextCheckDate = nextCheckDate;
            MaintainedFacilityID = maintainedFacilityId;
            AssociatedDevice = associatedDevice;
            Indications = new List<Indication>();
        }

        [Key]
        public int ID { get; set; }

        public CounterType Type { get; set; }
        public CounterSubType SubType { get; set; }
        public string ExtraType { get; set; }

        public string Model { get; set; }

        //[StringLength(32)]
        public string Number { get; set; }

        public DateTime NextCheckDate { get; set; }

        public List<Indication> Indications { get; set; }

        [ForeignKey("AssociatedDevice")]
        public int? AssociatedDeviceID { get; set; }
        public ConsumptionCounter AssociatedDevice { get; set; }

        //[ForeignKey("MaintainedFacility")]
        public int MaintainedFacilityID { get; set; }
        public MaintainedFacility MaintainedFacility { get; set; }

        public string ExtraValue { get; set; }

        [NotMapped]
        public string FullTypeShort => Type.GetDisplayName() + SubType.GetDisplayName() + " " + ExtraType;

        [NotMapped]
        public string FullTypeShortNoExtra => Type.GetDisplayName() + SubType.GetDisplayName();

        [NotMapped]
        public string FullType => Type == CounterType.HeatEnergyNodeMeasuringModule || Type == CounterType.HeatEnergyNodeThermoResistance ? ShortType : "Узел учета " + ShortType;

        [NotMapped]
        public string ShortType => Type.GetDescription()+ " " + SubType.GetDescription();

        [NotMapped]
        public bool HaveCheckDatesInThisMonth
        {
            get
            {
                var now = DateTime.Now;
                var nowPlusMonth = DateTime.Now.AddMonths(1);
                //bool AssociatedHaveCheckdate = ContextFactory.Instance.ConsumptionCounters.Any(c => c.AssociatedDeviceID == this.ID && now < c.NextCheckDate && c.NextCheckDate < nowPlusMonth);
                bool AssociatedHaveCheckdate = ContextFactory.Instance.ConsumptionCounters.Local.Any(c => c.AssociatedDeviceID == ID && now < c.NextCheckDate && c.NextCheckDate < nowPlusMonth);
                return AssociatedHaveCheckdate || (NextCheckDate.Year == now.Year && NextCheckDate.Month == now.Month);
            }
        }

        public double IndicationsForDate(DateTime date)
        {
            if (!ContextFactory.Instance.Entry(this).Collection(c => c.Indications).IsLoaded)
            {
                ContextFactory.Instance.Entry(this).Collection(c => c.Indications).Load();
            }
            var value = Indications.SingleOrDefault(i => i.Date.Year == date.Year && i.Date.Month == date.Month)?.Value;
            return value ?? new double();
        }

        public double ConsumtionForDate(DateTime date, bool WaterCirculation = false)
        {
            if (!ContextFactory.Instance.Entry(this).Collection(c => c.Indications).IsLoaded)
            {
                //TODO оптимизировать загрузку показаний
                ContextFactory.Instance.Entry(this).Collection(c => c.Indications).Load();
            }
            if (!WaterCirculation && (IndicationsForDate(date) == 0d || IndicationsForDate(date.AddMonths(-1)) == 0d)) return 0d;
            var multiplyer = 1d;
            if (Type == CounterType.ElectricNode)
            {
                var multiplyerString = ContextFactory.Instance.ConsumptionCounters.Where(c => c.AssociatedDeviceID == ID && c.Type == CounterType.CurrentTransformer).Select(c => c.ExtraValue).SingleOrDefault()?.Replace('.', ',');
                multiplyer = !string.IsNullOrEmpty(multiplyerString) ? double.Parse(multiplyerString) : 1;
            }
            //Console.WriteLine($"-----[{WaterCirculation}]-----");
            if (WaterCirculation)
            {
                //Console.WriteLine("************************************+");
                ConsumptionCounter suplyCounter = this;
                ConsumptionCounter returnCounter;
                if (SubType == CounterSubType.DigitalСirc)
                {
                    suplyCounter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.AssociatedDeviceID == ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule && c.SubType == CounterSubType.Suply);
                    returnCounter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.AssociatedDeviceID == ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule && c.SubType == CounterSubType.Return);
                }
                else if (SubType == CounterSubType.MechanicalСirc)
                {
                    returnCounter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.AssociatedDeviceID == ID && c.Type == CounterType.HotWaterNode && c.SubType == CounterSubType.Return);
                }
                else
                {
                    throw new ArgumentException();
                }


                var suplyLastValue    = suplyCounter.IndicationsForDate(date.AddMonths(-1));
                var suplyCurrentValue = suplyCounter.IndicationsForDate(date);

                var returnLastValue    = returnCounter.IndicationsForDate(date.AddMonths(-1));
                var returnCurrentValue = returnCounter.IndicationsForDate(date);

                if (suplyLastValue == 0d || suplyCurrentValue == 0d || returnLastValue == 0d || returnCurrentValue == 0d) return 0d;

                //Console.WriteLine($@"({suplyCurrentValue} - {suplyLastValue}) - ({returnCurrentValue} - {returnLastValue}) = {(suplyCurrentValue - suplyLastValue) - (returnCurrentValue - returnLastValue)}");
                return (suplyCurrentValue - suplyLastValue) - ( returnCurrentValue - returnLastValue);
            }
            else
            {
                var lastValue = IndicationsForDate(date.AddMonths(-1));
                var currentValue = IndicationsForDate(date);
                return (currentValue - lastValue) * multiplyer;
            }
        }

        public DataTable GetIndicationsTableForPeriod(DateTime periodStart, DateTime periodEnd)
        {
            if (!ContextFactory.Instance.Entry(this).Collection(c => c.Indications).IsLoaded)
            {
                ContextFactory.Instance.Entry(this).Collection(c =>c.Indications).Load();
            }
            periodStart = periodStart.AddDays(-(periodStart.Day - 1));
            periodEnd = periodEnd.AddDays(-(periodEnd.Day - 1));

            var dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("Год"));
            for (int i = 1; i <= 12; i++)
            {
                dataTable.Columns.Add(new DataColumn(new DateTime(2000, i, 1).ToString("MMMM")));
            }
            for (int year = periodStart.Year; year <= periodEnd.Year; year++)
            {
                DataRow valuesRow = dataTable.NewRow();
                valuesRow[0] = year;
                for (int month = 1; month <= 12; month++)
                {
                    if (new DateTime(year, month, 1)>= periodStart && new DateTime(year, month, 1) <= periodEnd)
                    {
                        var currentMonthValue = Indications.SingleOrDefault(i => i.Date.Year == year && i.Date.Month == month)?.Value;
                        valuesRow[month] = currentMonthValue != null ? $"{currentMonthValue:F}" /*+ ValueUnit*/ : "--";
                    }
                    else
                    {
                        valuesRow[month] = "--";
                    }
                }
                dataTable.Rows.Add(valuesRow);
            }
            return dataTable;
        }

        public DataTable GetConsumptionTableForPeriod(DateTime periodStart, DateTime periodEnd, bool WaterCirculation = false)
        {
            if (!ContextFactory.Instance.Entry(this).Collection(c => c.Indications).IsLoaded)
            {
                ContextFactory.Instance.Entry(this).Collection(c => c.Indications).Load();
            }
            periodStart = periodStart.AddDays(-(periodStart.Day - 1));
            periodEnd = periodEnd.AddDays(-(periodEnd.Day - 1));

            var dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("Год"));
            for (int i = 1; i <= 12; i++)
            {
                dataTable.Columns.Add(new DataColumn(new DateTime(2000, i, 1).ToString("MMMM")));
            }
            for (int year = periodStart.Year; year <= periodEnd.Year; year++)
            {
                DataRow valuesRow = dataTable.NewRow();
                valuesRow[0] = year;
                for (int month = 1; month <= 12; month++)
                {
                    if (new DateTime(year, month, 1) >= periodStart && new DateTime(year, month, 1) <= periodEnd)
                    {
                        //Console.WriteLine("------------------------------------------------------++");
                        //var lastMonthDate = new DateTime(year, month, 1).AddMonths(-1);
                        //var lastMonthValue = Indications.SingleOrDefault(i => i.Date.Year == lastMonthDate.Year && i.Date.Month == lastMonthDate.Month)?.Value;
                        var currentMonthValue = ConsumtionForDate(new DateTime(year, month, 1), WaterCirculation);
                        valuesRow[month] = currentMonthValue != 0d ? $"{currentMonthValue:F}" /*+ ValueUnit*/ : "--";
                    }
                    else
                    {
                        valuesRow[month] = "--";
                    }
                }
                dataTable.Rows.Add(valuesRow);
            }
            return dataTable;
        }

        public List<double> GetAverageConsumptionForYear(bool WaterCirculation = false)
        {
            List<double> averageValue = new List<double>();
            for (int month = 1; month <= 12; month++)
            {
                var tempDates = Indications.Where(i => i.Date.Year < DateTime.Now.Year && i.Date.Month == month).Select(i => i.Date);
                List<double> tempValues = new List<double>();
                foreach (var date in tempDates)
                {
                    var tempValue = ConsumtionForDate(date, WaterCirculation);
                    if (tempValue != 0d) tempValues.Add(tempValue);
                }
                int tempCount = tempValues.Any() ? tempValues.Count : 1;
                double? tempSumm = tempValues.Any() ? tempValues.Sum() : 0d;
                averageValue.Add((double)tempSumm / tempCount);
            }
            return averageValue;
        }

        public List<double> GetConsumptionForYear(DateTime date, bool WaterCirculation = false)
        {
            List<double> currentValues = new List<double>();
            for (int month = 1; month <= 12; month++)
            {
                double temp = ConsumtionForDate(new DateTime(date.Year, month, 1), WaterCirculation);
                currentValues.Add(temp);
            }
            return currentValues;
        }
    }
}
