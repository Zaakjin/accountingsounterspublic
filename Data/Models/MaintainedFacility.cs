﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using AccountingСounters.Database;
using AccountingСounters.Enums;

namespace AccountingСounters.Models
{
    public class MaintainedFacility
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string ManagerName { get; set; }

        //[StringLength(16)]
        public string ManagerPhone { get; set; }

        //[StringLength(16)]
        public string SentryPhone { get; set; }

        public string IndicationsLiableName { get; set; }

        //[StringLength(16)]
        public string IndicationsLiablePhone { get; set; }

        public List<ConsumptionCounter> Counters { get; set; }



        [NotMapped]
        public string GetManagerInfo => ManagerName + "\n" + ManagerPhone;

        [NotMapped]
        public string GetLiableInfo => IndicationsLiableName + "\n" + IndicationsLiablePhone;

        [NotMapped]
        public string GetHeatEnergyNodeCount  => Counters.Count(c => c.Type == CounterType.HeatEnergyNode).ToString();

        [NotMapped]
        public string GetColdWaterNodeCount   => Counters.Count(c => c.Type == CounterType.ColdWaterNode).ToString();

        [NotMapped]
        public string GetHotWaterNodeCount    => Counters.Count(c => c.Type == CounterType.HotWaterNode).ToString();

        [NotMapped]
        public string GetElectricityNodeCount => Counters.Count(c => c.Type == CounterType.ElectricNode).ToString();



        [NotMapped]
        public bool HaveCheckDatesInThisMonth
        {
            get
            {
                if (!ContextFactory.Instance.Entry(this).Collection(c => c.Counters).IsLoaded)
                {
                    ContextFactory.Instance.Entry(this).Collection(c => c.Counters).Load();
                }
                return Counters.Any(c => c.NextCheckDate.Year == DateTime.Now.Year && c.NextCheckDate.Month == DateTime.Now.Month);
            }
        }
    }
}
