﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountingСounters.Models
{
    public class Indication
    {
        public Indication(double value, ConsumptionCounter consumptionCounter, DateTime date)
        {
            Value = value;
            ConsumptionCounter = consumptionCounter;
            Date = date;
        }

        public Indication()
        {}

        [Key]
        public int ID { get; set; }

        public double Value { get; set; }
        
        [Required]
        [ForeignKey("ConsumptionCounter")]
        public int ConsumptionCounterID { get; set; }
        public ConsumptionCounter ConsumptionCounter { get; set; }

        public DateTime Date { get; set; }

        public bool IsSameMonth(DateTime dateCompare)
        {
            return Date.Year == dateCompare.Year && Date.Month == dateCompare.Month;
        }
    }
}
