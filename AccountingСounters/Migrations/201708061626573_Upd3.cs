using System.Data.Entity.Migrations;

namespace AccountingСounters.Migrations
{
    public partial class Upd3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConsumptionCounters", "SubType", c => c.Int(nullable: false));
            DropColumn("dbo.ConsumptionCounters", "SubTypeType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ConsumptionCounters", "SubTypeType", c => c.Int(nullable: false));
            DropColumn("dbo.ConsumptionCounters", "SubType");
        }
    }
}
