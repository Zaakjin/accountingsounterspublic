using System.Data.Entity.Migrations;

namespace AccountingСounters.Migrations
{
    public partial class upd61 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConsumptionCounters", "ExtraValue", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConsumptionCounters", "ExtraValue");
        }
    }
}
