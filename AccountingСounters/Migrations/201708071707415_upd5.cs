using System.Data.Entity.Migrations;

namespace AccountingСounters.Migrations
{
    public partial class upd5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Indications", "Value", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Indications", "Value");
        }
    }
}
