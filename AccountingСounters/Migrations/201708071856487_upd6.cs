using System.Data.Entity.Migrations;

namespace AccountingСounters.Migrations
{
    public partial class upd6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Indications", "Value", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Indications", "Value", c => c.Int(nullable: false));
        }
    }
}
