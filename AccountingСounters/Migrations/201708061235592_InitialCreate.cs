using System.Data.Entity.Migrations;

namespace AccountingСounters.Migrations
{
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConsumptionCounters",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        SubTypeType = c.Int(nullable: false),
                        Model = c.String(),
                        Number = c.String(maxLength: 32),
                        NextCheckDate = c.DateTime(nullable: false),
                        MaintainedFacilityID = c.Int(nullable: false),
                        AssociatedDevice_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ConsumptionCounters", t => t.AssociatedDevice_ID)
                .ForeignKey("dbo.MaintainedFacilities", t => t.MaintainedFacilityID, cascadeDelete: true)
                .Index(t => t.MaintainedFacilityID)
                .Index(t => t.AssociatedDevice_ID);
            
            CreateTable(
                "dbo.Indications",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ConsumptionCounterID = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ConsumptionCounters", t => t.ConsumptionCounterID, cascadeDelete: true)
                .Index(t => t.ConsumptionCounterID);
            
            CreateTable(
                "dbo.MaintainedFacilities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        ManagerName = c.String(),
                        ManagerPhone = c.String(maxLength: 16),
                        SentryPhone = c.String(maxLength: 16),
                        IndicationsLiableName = c.String(),
                        IndicationsLiablePhone = c.String(maxLength: 16),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ConsumptionCounters", "MaintainedFacilityID", "dbo.MaintainedFacilities");
            DropForeignKey("dbo.Indications", "ConsumptionCounterID", "dbo.ConsumptionCounters");
            DropForeignKey("dbo.ConsumptionCounters", "AssociatedDevice_ID", "dbo.ConsumptionCounters");
            DropIndex("dbo.Indications", new[] { "ConsumptionCounterID" });
            DropIndex("dbo.ConsumptionCounters", new[] { "AssociatedDevice_ID" });
            DropIndex("dbo.ConsumptionCounters", new[] { "MaintainedFacilityID" });
            DropTable("dbo.MaintainedFacilities");
            DropTable("dbo.Indications");
            DropTable("dbo.ConsumptionCounters");
        }
    }
}
