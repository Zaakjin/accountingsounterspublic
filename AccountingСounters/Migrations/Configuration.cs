using System.Data.Entity.Migrations;

namespace AccountingСounters.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<AccountingСounters.Database.DataModel>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "AccountingСounters.Database.DataModel";
        }

        protected override void Seed(AccountingСounters.Database.DataModel context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
