using System.Data.Entity.Migrations;

namespace AccountingСounters.Migrations
{
    public partial class Upd2 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ConsumptionCounters", name: "AssociatedDevice_ID", newName: "AssociatedDeviceID");
            RenameIndex(table: "dbo.ConsumptionCounters", name: "IX_AssociatedDevice_ID", newName: "IX_AssociatedDeviceID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.ConsumptionCounters", name: "IX_AssociatedDeviceID", newName: "IX_AssociatedDevice_ID");
            RenameColumn(table: "dbo.ConsumptionCounters", name: "AssociatedDeviceID", newName: "AssociatedDevice_ID");
        }
    }
}
