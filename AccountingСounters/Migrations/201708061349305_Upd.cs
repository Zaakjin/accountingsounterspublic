using System.Data.Entity.Migrations;

namespace AccountingСounters.Migrations
{
    public partial class Upd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConsumptionCounters", "ExtraType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConsumptionCounters", "ExtraType");
        }
    }
}
