﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace AccountingСounters.Helpers.XLHelpers
{
    public static class XLExtensions
    {
        public static void SetAllBorders(this IXLRange range, XLBorderStyleValues borderStyle)
        {

            range.Style.Border.InsideBorder = borderStyle;
            range.Style.Border.OutsideBorder = borderStyle;
        }
    }
}
