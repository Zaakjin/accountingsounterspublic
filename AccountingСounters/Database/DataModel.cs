﻿using System.Data.Entity;
using System.Data.SQLite;
using AccountingСounters.Models;
using SQLite.CodeFirst;

namespace AccountingСounters.Database
{
    public class DataModel : DbContext
    {
        // Your context has been configured to use a 'DataModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'AccountingСounters.DataModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'DataModel' 
        // connection string in the application configuration file.
        //public DataModel() : base("name=DataModel")
        //{
        //}

        //public DataModel() : base("Data Source=(local)\\SQLEXPRESS;Initial Catalog=AccountingCounters;Integrated Security=True")
        //{
        //}

        //public DataModel() : base(Properties.Settings.Default.ConnetionStringName)
        //{
        //}

        public DataModel() : base("SQLiteDatabase")
        {
        }

        public DataModel(SQLiteConnection connection) : base(connection,false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var sqliteConnectionInitializer = new SqliteCreateDatabaseIfNotExists<DataModel>(modelBuilder);
            System.Data.Entity.Database.SetInitializer(sqliteConnectionInitializer);
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }

        public DbSet<MaintainedFacility> MaintainedFacilities { get; set; }

        public DbSet<ConsumptionCounter> ConsumptionCounters { get; set; }

        public DbSet<Indication> Indications { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}