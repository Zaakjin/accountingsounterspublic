﻿using System;
using System.Data.Entity.Infrastructure;
using AccountingСounters.Properties;

namespace AccountingСounters.Database
{
    class ContextFactory : IDbContextFactory<DataModel>
    {
        public static string ConnectionString { get; private set; }

        //public static SqlConnectionStringBuilder SetConnectionParameters()
        //{
        //    var connectionStringBuilder = new SqlConnectionStringBuilder
        //    {
        //        InitialCatalog = "AccountingCounters",
        //        IntegratedSecurity = true
        //    };

        //    ConnectionString = connectionStringBuilder.ToString();
        //    return connectionStringBuilder;
        //}

        //public static string GetConnectionParameters()
        //{
        //    var connectionStringBuilder = new SqlConnectionStringBuilder
        //    {
        //        InitialCatalog = "AccountingCounters",
        //        IntegratedSecurity = true
        //    };

        //    ConnectionString = connectionStringBuilder.ToString();
        //    return ConnectionString;
        //}

        private static DataModel _instance;
        private static Object thisLock = new Object();

        public static DataModel Instance
        {
            get
            {
                lock (thisLock)
                {
                    if (_instance != null) return _instance;
                    _instance = new ContextFactory().Create();
                    if(Settings.Default.ConsoleOutSQLRequests) _instance.Database.Log = x => Console.WriteLine(x);
                    //System.Data.Entity.Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DataModel>());
                    //System.Data.Entity.Database.SetInitializer(new CreateDatabaseIfNotExists<DataModel>());
                    return _instance;
                }
            }
            set => _instance = value;
        }

        //public static DataModel InstanceLog
        //{
        //    get
        //    {
        //        lock (thisLock)
        //        {
        //            if (_instance != null)
        //            {

        //                _instance.Database.Log = x => Console.WriteLine(x);
        //                return _instance;
        //            }
        //            _instance = new ContextFactory().Create();
        //            //System.Data.Entity.Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DataModel>());
        //            //System.Data.Entity.Database.SetInitializer(new CreateDatabaseIfNotExists<DataModel>());
        //            return _instance;
        //        }
        //    }
        //    set => _instance = value;
        //}

        public DataModel Create()
        {
            //SetConnectionParameters();
            //if (string.IsNullOrEmpty(ConnectionString)) throw new InvalidOperationException("Please set the connection parameters before trying to instantiate a database connection.");

            return new DataModel();
        }

        public static void Reset()
        {
            _instance = null;
        }
    }
}
