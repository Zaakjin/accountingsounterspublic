﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;
using AccountingСounters.Database;
using AccountingСounters.Models;
using LiveCharts;
using LiveCharts.Wpf;

namespace AccountingСounters.CustomControls
{
    /// <summary>
    /// Interaction logic for IndicationNodeGraph.xaml
    /// </summary>
    public partial class IndicationNodeGraph : UserControl
    {

        public ConsumptionCounter Counter;
        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }
        public bool WaterCirculation { get; set; } = false;

        public IndicationNodeGraph(ConsumptionCounter counter)
        {
            InitializeComponent();

            Counter = counter;

            UpdateValues();

            Labels = new[] { "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"};

            DataContext = this;
        }

        public void UpdateValues()
        {
            //Загружаем показания счетчика на всякий случай
            if (!ContextFactory.Instance.Entry(Counter).Collection(c => c.Indications).IsLoaded)
            {
                ContextFactory.Instance.Entry(Counter).Collection(c => c.Indications).Load();
            }
            //Получаем расход за текущий год
            List<double> currentValues = Counter.GetConsumptionForYear(DateTime.Now, WaterCirculation);
            for (int i = 0; i < currentValues.Count; i++)
            {
                currentValues[i] = Math.Round(currentValues[i], 2);
            }

            #region OldCode1
            //List<double> currentValues = new List<double>();
            //for (int month = 1; month <= 12; month++)
            //{
            //    double temp = Counter.ConsumtionForDate(new DateTime(DateTime.Now.Year, month, 1));
            //    currentValues.Add(temp);
            //}
            #endregion

            //Получаем средний расход за все время
            List<double> averageValue = Counter.GetAverageConsumptionForYear(WaterCirculation);
            for (int i = 0; i < averageValue.Count; i++)
            {
                averageValue[i] = Math.Round(averageValue[i], 2);
            }
            #region OldCode2
            //List<double> averageValue = new List<double>();
            //for (int month = 1; month <= 12; month++)
            //{
            //    var tempDates = Counter.Indications.Where(i => i.Date.Year < DateTime.Now.Year && i.Date.Month == month).Select(i => i.Date);
            //    List<double> tempValues = new List<double>();
            //    foreach (var date in tempDates)
            //    {
            //        var tempValue = Counter.ConsumtionForDate(date);
            //        if(tempValue != 0d) tempValues.Add(tempValue);
            //    }
            //    int tempCount = tempValues.Any() ? tempValues.Count() : 1;
            //    double? tempSumm = tempValues.Any() ? tempValues.Sum() : 0d;
            //    averageValue.Add((double)tempSumm / tempCount);
            //} 
            #endregion
            LiveCharts.Wpf.Charts.Base.Chart.Colors = new List<Color>()
            {
                Colors.Coral,
                Colors.CornflowerBlue
            };
            
            if(SeriesCollection == null) SeriesCollection = new SeriesCollection();
            else SeriesCollection?.Clear();
            SeriesCollection.Add(new LineSeries
            {
                Title = "Ср. расход",
                LineSmoothness = 0.5,
                Values = new ChartValues<double>(averageValue)

            });
            SeriesCollection.Add(new LineSeries
            {
                Title = "Тек. расход",
                LineSmoothness = 0.5,
                Values = new ChartValues<double>(currentValues)
            });
            DataContext = this;
        }
    }
}
