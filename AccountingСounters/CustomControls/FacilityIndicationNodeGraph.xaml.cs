﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using AccountingСounters.Database;
using AccountingСounters.Enums;
using AccountingСounters.Models;
using LiveCharts;
using LiveCharts.Wpf;

namespace AccountingСounters.CustomControls
{
    /// <summary>
    /// Interaction logic for FacilityIndicationNodeGraph.xaml
    /// </summary>
    public partial class FacilityIndicationNodeGraph : UserControl
    {

        public MaintainedFacility Facility;
        public SeriesCollection SeriesCollection { get; set; }
        private SeriesCollection _coldWaterSeriesCollection;
        private SeriesCollection _hotWaterSeriesCollection;
        private SeriesCollection _heatEnergySeriesCollection;
        private SeriesCollection _electricitySeriesCollection;
        public string[] Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }

        public SeriesCollection ColdWaterSeriesCollection
        {
            get => _coldWaterSeriesCollection;
            set => _coldWaterSeriesCollection = value;
        }

        public SeriesCollection HotWaterSeriesCollection
        {
            get => _hotWaterSeriesCollection;
            set => _hotWaterSeriesCollection = value;
        }

        public SeriesCollection HeatEnergySeriesCollection
        {
            get => _heatEnergySeriesCollection;
            set => _heatEnergySeriesCollection = value;
        }

        public SeriesCollection ElectricitySeriesCollection
        {
            get => _electricitySeriesCollection;
            set => _electricitySeriesCollection = value;
        }

        public FacilityIndicationNodeGraph(MaintainedFacility facility)
        {
            InitializeComponent();

            Facility = facility;

            UpdateValues();

            Labels = new[] { "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" };

            LiveCharts.Wpf.Charts.Base.Chart.Colors = new List<Color>()
            {
                Colors.Coral,
                Colors.CornflowerBlue
            };

            DataContext = this;
        }

        public void UpdateValues()
        {

            //Загружаем показания счетчика на всякий случай
            if (!ContextFactory.Instance.Entry(Facility).Collection(c => c.Counters).IsLoaded)
            {
                ContextFactory.Instance.Entry(Facility).Collection(c => c.Counters).Load();
            }
            foreach (var counter in Facility.Counters)
            {
                if (!ContextFactory.Instance.Entry(counter).Collection(c => c.Indications).IsLoaded)
                {
                    ContextFactory.Instance.Entry(counter).Collection(c => c.Indications).Load();
                }
            }
            //Получаем расход холодной воды
            var ColdWaterValues = GetFacilityValuesOfType(CounterType.ColdWaterNode);
            //Получаем расход горячей воды
            var HotWaterValues = GetFacilityValuesOfType(CounterType.HotWaterNode);
            //Получаем расход тепла
            var HeatEnergyValues = GetFacilityValuesOfType(CounterType.HeatEnergyNode);
            //Получаем расход электричества
            var ElectricityValues = GetFacilityValuesOfType(CounterType.ElectricNode);

            //Иницаиализируем значения графиков
            InitSeriesCollection(ref _coldWaterSeriesCollection, ColdWaterValues);
            InitSeriesCollection(ref _hotWaterSeriesCollection, HotWaterValues);
            InitSeriesCollection(ref _heatEnergySeriesCollection, HeatEnergyValues);
            InitSeriesCollection(ref _electricitySeriesCollection, ElectricityValues);

            //DataContext = this;
        }

        public void InitSeriesCollection(ref SeriesCollection collection, List<double>[] list)
        {
            //MessageBox.Show(list[0].ToArray().ToString()+"");
            if (collection == null) collection = new SeriesCollection();
            else collection?.Clear();
            collection.Add(new LineSeries
            {
                Title = "Ср. расход",
                LineSmoothness = 0.5,
                Values = new ChartValues<double>(list[0])

            });
            collection.Add(new LineSeries
            {
                Title = "Тек. расход",
                LineSmoothness = 0.5,
                Values = new ChartValues<double>(list[1])
            });
        }

        public List<double>[] GetFacilityValuesOfType(CounterType type)
        {
            List<double> averageValues = new List<double>();
            List<double> currentValues = new List<double>();
            List<List<double>> averageValuesSeparateCounters = new List<List<double>>();
            List<List<double>> currentValuesSeparateCounters = new List<List<double>>();
            var counters = Facility.Counters.Where(c => c.Type == type);
            if (counters == null || !counters.Any())
            {
                for (int i = 0; i <= 11; i++)
                {
                    averageValues.Add(0);
                }
                return new[] {averageValues, averageValues};
            }
            foreach (var counter in counters)
            {
                averageValuesSeparateCounters.Add(counter.GetAverageConsumptionForYear());
                currentValuesSeparateCounters.Add(counter.GetConsumptionForYear(DateTime.Now));
            }
            //MessageBox.Show(averageValuesSeparateCounters.Count.ToString());
            for (int i = 0; i <= 11; i++)
            {
                averageValues.Add(averageValuesSeparateCounters.Select(c => c[i]).Sum());
                currentValues.Add(currentValuesSeparateCounters.Select(c => c[i]).Sum());
            }
            return new [] {averageValues, currentValues};
        }
    }
}
