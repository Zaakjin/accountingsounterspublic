﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AccountingСounters.Database;
using AccountingСounters.Enums;
using AccountingСounters.Models;

namespace AccountingСounters.CustomControls
{
    /// <summary>
    /// Interaction logic for CurrentTransformatorInfo.xaml
    /// </summary>
    public partial class CurrentTransformatorInfo : UserControl
    {
        private bool _isReadOnly;
        private ConsumptionCounter _transformator;

        public string Type
        {
            get => typeTextBox.Text;
            set => typeTextBox.Text = value;
        }

        public string Model
        {
            get => modelTextBox.Text;
            set => modelTextBox.Text = value;
        }

        public string Numbers
        {
            get => numberTextBox.Text +"//" + numberTextBox2.Text + "//" + numberTextBox3.Text;
            set
            {
                string[] numArray = value.Split(new[] {"//"}, StringSplitOptions.None);
                Number1 = numArray[0];
                if (numArray.Length == 3)
                {
                    Number2 = numArray[1];
                    Number3 = numArray[2];
                }
            }
        }

        public string Number1
        {
            get => numberTextBox.Text;
            set => numberTextBox.Text = value;
        }

        public string Number2
        {
            get => numberTextBox2.Text;
            set => numberTextBox2.Text = value;
        }

        public string Number3
        {
            get => numberTextBox3.Text;
            set => numberTextBox3.Text = value;
        }

        //TODO
        public DateTime? CheckDate
        {
            get => checkDatePicker.SelectedDate;
            set => checkDatePicker.SelectedDate = value;
        }

        public double TransformationCoefficient
        {
            get => (double) transformationCoefficient.Value;
            set => transformationCoefficient.Value = value;
        }

        private ConsumptionCounter Transformator
        {
            get => _transformator;
            set
            {
                _transformator = value;
                Type = value.ExtraType;
                Model = value.Model;
                Numbers = value.Number;
                CheckDate = value.NextCheckDate;
                TransformationCoefficient = double.Parse(value.ExtraValue.Replace('.', ','));
            }
        }

        public int FacilityID { get; set; }
        public ConsumptionCounter AssociatedDevice { get; set; }

        public bool IsInputValid => !(Type == "" || Model == "" || Numbers == "" || CheckDate == null);

        public bool IsReadOnly
        {
            get => _isReadOnly;
            set
            {
                _isReadOnly = value;
                typeTextBox.IsReadOnly = _isReadOnly;
                modelTextBox.IsReadOnly = _isReadOnly;
                numberTextBox.IsReadOnly = _isReadOnly;
                transformationCoefficient.IsReadOnly = _isReadOnly;
            }
        }

        public CurrentTransformatorInfo()
        {
            InitializeComponent();
        }

        public CurrentTransformatorInfo(int facilityID, ConsumptionCounter associatedDeviceID)
        {
            InitializeComponent();
            FacilityID = facilityID;
            AssociatedDevice = associatedDeviceID;
        }

        public CurrentTransformatorInfo(ConsumptionCounter transformator)
        {
            InitializeComponent();
            Transformator = transformator;
            FacilityID = transformator.MaintainedFacilityID;
            AssociatedDevice = transformator.AssociatedDevice;
        }

        public void Save()
        {
            ConsumptionCounter tempConsumptionCounter;
            if (_transformator == null)
            {
                tempConsumptionCounter = new ConsumptionCounter(CounterType.CurrentTransformer, CounterSubType.Default, Type, Model, Numbers, (DateTime) CheckDate, FacilityID, AssociatedDevice);
                ContextFactory.Instance.ConsumptionCounters.Add(tempConsumptionCounter);
            }
            else
            {
                tempConsumptionCounter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.ID == _transformator.ID);
                tempConsumptionCounter.ExtraType = Type;
                tempConsumptionCounter.Model = Model;
                tempConsumptionCounter.Number = Numbers;
                tempConsumptionCounter.NextCheckDate = (DateTime) CheckDate;
            }
            ContextFactory.Instance.SaveChanges();

        }

        private void expander_Collapsed(object sender, RoutedEventArgs e)
        {
            ((Expander)sender).VerticalAlignment = VerticalAlignment.Top;
        }

        private void expander_Expanded(object sender, RoutedEventArgs e)
        {
            ((Expander)sender).VerticalAlignment = VerticalAlignment.Stretch;
        }
    }
}
