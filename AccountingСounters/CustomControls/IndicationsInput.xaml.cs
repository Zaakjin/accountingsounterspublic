﻿using System;
using System.Linq;
using System.Windows.Controls;
using AccountingСounters.Database;
using AccountingСounters.Models;

namespace AccountingСounters.CustomControls
{
    /// <summary>
    /// Interaction logic for IndicationsInput.xaml
    /// </summary>
    public partial class IndicationsInput : UserControl
    {
        public DateTime Date { get; set; }
        public ConsumptionCounter Counter {get; }
        public string Value => currentIndication.Text;
        public IndicationsInput(ConsumptionCounter counter, DateTime initialDateTime)
        {
            InitializeComponent();
            Counter = counter;
            Date = initialDateTime;

            var title = counter.FullType + " №: " + counter.Number;
            CounterLabel.Content = title;

            UpdateLastIndication();
        }

        public void UpdateLastIndication()
        {
            var date = Date.AddMonths(-1);
            lastIndication.Text = ContextFactory.Instance.Indications.SingleOrDefault(i => i.ConsumptionCounterID == Counter.ID &&
                                                                                           i.Date.Year == date.Year &&
                                                                                           i.Date.Month == date.Month)?.Value.ToString() ?? "--";
        }

        public void SaveCounterIndication()
        {
            var tempIndication = ContextFactory.Instance.Indications.SingleOrDefault(i => i.ConsumptionCounterID == Counter.ID && i.Date.Year == Date.Year && i.Date.Month == Date.Month);
            if (tempIndication != null)
            {
                //ContextFactory.Instance.Indications.Remove(tempIndication);
                tempIndication.Value = double.Parse(currentIndication.Text.Replace('.', ','));
            }
            else
            {
                ContextFactory.Instance.Indications.Add(new Indication(double.Parse(currentIndication.Text.Replace('.', ',')), Counter, Date));
            }
            ContextFactory.Instance.SaveChanges();
        }
    }
}
