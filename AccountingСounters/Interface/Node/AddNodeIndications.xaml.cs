﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using AccountingСounters.Database;
using AccountingСounters.Models;

namespace AccountingСounters.Interface.Node
{
    /// <summary>
    /// Interaction logic for AddNodeIndications.xaml
    /// </summary>
    public partial class AddNodeIndications : Window
    {
        private ConsumptionCounter consumptionCounter;
        public AddNodeIndications(ConsumptionCounter consumptionCounter)
        {
            InitializeComponent();
            this.consumptionCounter = consumptionCounter;
            var title = consumptionCounter.FullType + " №: " + consumptionCounter.Number;
            this.Title = title;
            NodeLabel.Content = title;
            monthPicker.SelectedDate = DateTime.Now;
            UpdateLastIndication();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            var datePickerDate = monthPicker.SelectedDate.Value;
            var tempIndication = ContextFactory.Instance.Indications.SingleOrDefault(i => i.ConsumptionCounterID == consumptionCounter.ID &&
                                                                                          i.Date.Year == datePickerDate.Year &&
                                                                                          i.Date.Month == datePickerDate.Month);
            if (tempIndication != null)
            {
                if (MessageBox.Show($"Показания за {monthPicker.SelectedDate.Value:MMMM-yyyy} уже внесены в базу данных. \nЗаменить?", "", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                {
                    return;
                }
                tempIndication.Value = double.Parse(currentIndication.Text.Replace('.', ','));
            }
            else
            {
                ContextFactory.Instance.Indications.Add(new Indication(double.Parse(currentIndication.Text.Replace('.',',')), consumptionCounter, monthPicker.SelectedDate.Value));
            }
            ContextFactory.Instance.SaveChanges();
            ((AccountingNodeInfo)Owner).UpdateGridData();
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void UpdateLastIndication()
        {
            var date = monthPicker.SelectedDate.Value.AddMonths(-1);
            lastIndication.Text = ContextFactory.Instance.Indications.SingleOrDefault(i => i.ConsumptionCounterID == consumptionCounter.ID &&
                                                                                           i.Date.Year == date.Year &&
                                                                                           i.Date.Month == date.Month)?.Value.ToString(CultureInfo.CurrentCulture) ?? "--";
        }

        private void monthPicker_SelectedDateChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            UpdateLastIndication();
        }
    }
}
