﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using AccountingСounters.CustomControls;
using AccountingСounters.Database;
using AccountingСounters.Enums;
using AccountingСounters.Interface.Facility;
using AccountingСounters.Models;

namespace AccountingСounters.Interface.Node
{
    /// <summary>
    /// Interaction logic for AccountingNodeInfo.xaml
    /// </summary>
    public partial class AccountingNodeInfo : Window
    {
        private ConsumptionCounter mainCounter;
        private IndicationNodeGraph graph;

        private CurrentTransformatorInfo CurrentTransformator;

        public ObservableCollection<YearIndication> ConsumptionTable { get; set; }
        public ObservableCollection<YearIndication> IndicationsTable { get; set; }
        public ObservableCollection<YearIndication> IndicationsTable2 { get; set; }

        private string[] MonthNames;

        private bool IndicationsTableChanged = false;
        private bool IndicationsTable2Changed = false;

        private IQueryable<ConsumptionCounter> associatedDevices;
        //private CurrentTransformatorInfo CurrentTransformator;

        public AccountingNodeInfo()
        {
            InitializeComponent();
            MonthNames = new string[13];
            MonthNames[0] = "Год";
            for (int i = 1; i <= 12; i++)
            {
                MonthNames[i]= new DateTime(2000, i, 1).ToString("MMMM");
            }
            DataContext = this;
        }

        public void PostInit()
        {
            mainCounter = (ConsumptionCounter)((FacilityCard)Owner).countersGrid.SelectedItem; //Получаем выбранный узел учета

            //Формируем название окна
            string moduleName = mainCounter.Type == CounterType.HeatEnergyNodeMeasuringModule || mainCounter.Type == CounterType.HeatEnergyNodeThermoResistance ? mainCounter.ShortType : "Узел учета " + mainCounter.ShortType;
            
            //Устанавливаем название окна
            Title = moduleName;
            
            //Устанавливаем название Груп Бокса основного счетчика
            if (mainCounter.Type == CounterType.HeatEnergyNode ||
                mainCounter.Type == CounterType.HotWaterNode &&
                (mainCounter.SubType == CounterSubType.DigitalСirc || mainCounter.SubType == CounterSubType.Digital))
            {
                accountingNode.Header = "Вычислитель";
            }

            //Если цифровой узел учета горячей воды заполняем список выбора типа показаний
            if (mainCounter.Type == CounterType.HotWaterNode &&
               (mainCounter.SubType == CounterSubType.DigitalСirc || mainCounter.SubType == CounterSubType.Digital))
            {
                indicationsType.ItemsSource = new List<string> { "Тепло", "Вода" };
                indicationsType.Visibility = Visibility.Visible;
                indicationsType.SelectedIndex = 0;
            }

            //Установка высоты третей таблицы
            //if (!(mainCounter.Type == CounterType.HotWaterNode && (mainCounter.SubType == CounterSubType.DigitalСirc || mainCounter.SubType == CounterSubType.MechanicalСirc)))
            //{
            //    indicationsGridRow3.Height = new GridLength(1, GridUnitType.Auto);
            //}
            
            //Устанавливаем информацию об основном счетчике
            counterExtraType.Text = mainCounter.ExtraType;
            counterModel.Text = mainCounter.Model;
            counterNumber.Text = mainCounter.Number;
            counterCheckDate.SelectedDate = mainCounter.NextCheckDate;

            //Загружаем информацию о доп модулях узла учета
            associatedDevices = ContextFactory.Instance.ConsumptionCounters.Where(c => c.AssociatedDeviceID == mainCounter.ID);

            //Устанавливаем информацию о доп модулях узла учета если Тепло или Цифр. ГВС
            if (mainCounter.Type == CounterType.HeatEnergyNode ||
                mainCounter.Type == CounterType.HotWaterNode && (mainCounter.SubType == CounterSubType.Digital || mainCounter.SubType == CounterSubType.DigitalСirc))
            {
                var measuringModules = associatedDevices.Where(c => /*c.AssociatedDeviceID == mainCounter.ID &&*/ c.Type == CounterType.HeatEnergyNodeMeasuringModule).ToArray();
                //Устанавливаем инф. о Изм. модуль 1
                measuringModule1ExtraType.Text = measuringModules[0].ExtraType;
                measuringModule1Model.Text = measuringModules[0].Model;
                measuringModule1Number.Text = measuringModules[0].Number;
                measuringModule1CheckDate.SelectedDate = measuringModules[0].NextCheckDate;

                var thermoresistances = associatedDevices.Where(c => /*c.AssociatedDeviceID == mainCounter.ID &&*/ c.Type == CounterType.HeatEnergyNodeThermoResistance).ToArray();
                //Если есть термосопротивления
                if (thermoresistances.Any())
                {
                    //Термосопротивление 1
                    thermoRes1ExtraType.Text = thermoresistances[0].ExtraType;
                    thermoRes1Model.Text = thermoresistances[0].Model;
                    thermoRes1Number.Text = thermoresistances[0].Number;
                    thermoRes1CheckDate.SelectedDate = thermoresistances[0].NextCheckDate;
                }

                //Если УУ Тепла или ГВС Цифр. Циркуляция устанавливаем инф о вторых доп модулях
                if (mainCounter.Type == CounterType.HeatEnergyNode ||
                    mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.DigitalСirc)
                {
                    //Устанавливаем инф. о Изм. модуль 1
                    measuringModule2ExtraType.Text = measuringModules[1].ExtraType;
                    measuringModule2Model.Text = measuringModules[1].Model;
                    measuringModule2Number.Text = measuringModules[1].Number;
                    measuringModule2CheckDate.SelectedDate = measuringModules[1].NextCheckDate;


                    //Если есть термосопротивления
                    if (thermoresistances.Any())
                    {
                        //Термосопротивление 2
                        thermoRes2ExtraType.Text = thermoresistances[1].ExtraType;
                        thermoRes2Model.Text = thermoresistances[1].Model;
                        thermoRes2Number.Text = thermoresistances[1].Number;
                        thermoRes2CheckDate.SelectedDate = thermoresistances[1].NextCheckDate;
                    }
                }
                accountingNode.Header = "Вычислитель";
                if (!thermoresistances.Any())
                {
                    expanderThermoRes.Visibility = Visibility.Collapsed;
                }
                if (mainCounter.SubType == CounterSubType.Digital)
                {
                    secondModuleColumn.Width = new GridLength(0);
                    secondThermoresColumn.Width = new GridLength(0);
                }
            }
            else
            {
                expanderMesuringModules.Visibility = Visibility.Collapsed;
                expanderThermoRes.Visibility = Visibility.Collapsed;
            }

            //Устанавливаем информацию о доп модулях узла учета если Мех. ГВС Циркуляция
            if (mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.MechanicalСirc)
            {
                accountingNode.Header = "Счетчик подача";
                SecondaryAccountingNode.Visibility = Visibility.Visible;
                var secondaryCounter = associatedDevices.Single(c => /*c.AssociatedDeviceID == mainCounter.ID &&*/ c.Type == CounterType.HotWaterNode && c.SubType == CounterSubType.Return);
                SecondaryCounterExtraType.Text = secondaryCounter.ExtraType;
                SecondaryCounterModel.Text = secondaryCounter.Model;
                SecondaryCounterNumber.Text = secondaryCounter.Number;
                SecondaryCounterCheckDate.SelectedDate = secondaryCounter.NextCheckDate;

                //Регестрируем эвенты изменения текста
                SecondaryCounterExtraType.TextChanged += textbox_TextChanged;
                SecondaryCounterModel.TextChanged += textbox_TextChanged;
                SecondaryCounterNumber.TextChanged += textbox_TextChanged;

                expanderIndicationsGrid2.Header = "Показания подача";
            }

            if (mainCounter.Type == CounterType.ElectricNode)
            {
                var transformator = associatedDevices.SingleOrDefault(c => /*c.AssociatedDeviceID == mainCounter.ID &&*/ c.Type == CounterType.CurrentTransformer);
                if (transformator != null)
                {
                    //CurrentTransformatorInfo CurrentTransformator = new CurrentTransformatorInfo();
                    CurrentTransformator = new CurrentTransformatorInfo(transformator);

                    innerDock.Children.Add(CurrentTransformator);
                    CurrentTransformator.checkDatePicker.SelectedDateChanged += transformatorCheckDate_SelectedDateChanged;

                    void transformatorCheckDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
                    {
                        if (e.RemovedItems.Count == 0) return;
                        ((DatePicker) sender).SelectedDateChanged -= transformatorCheckDate_SelectedDateChanged;
                        if (MessageBox.Show("Установить новую дату следующей поверки?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        {
                            ((DatePicker) sender).SelectedDate = (DateTime) e.RemovedItems[0];
                            ((DatePicker) sender).SelectedDateChanged += transformatorCheckDate_SelectedDateChanged;
                            return;
                        }
                        transformator.NextCheckDate = (DateTime) e.AddedItems[0];
                        ContextFactory.Instance.SaveChanges();
                        ((DatePicker) sender).SelectedDateChanged += transformatorCheckDate_SelectedDateChanged;
                        ((FacilityCard) Owner).UpdateData();
                        App.AppMainWindow.SetUpdateData();
                    }

                    //Регестрируем эвенты изменения текста
                    CurrentTransformator.typeTextBox.TextChanged += textbox_TextChanged;
                    CurrentTransformator.modelTextBox.TextChanged += textbox_TextChanged;
                    CurrentTransformator.numberTextBox.TextChanged += textbox_TextChanged;
                    CurrentTransformator.numberTextBox2.TextChanged += textbox_TextChanged;
                    CurrentTransformator.numberTextBox3.TextChanged += textbox_TextChanged;
                }
            }
            /*
             ТАБЛИЦЫ
             */
            //Скрыть таблицу показаний, график показаний и кнопку ввода показаний если это измерительный модуль, термосопротивление или трансформатор тока
            if (new HashSet<CounterType> {CounterType.HeatEnergyNodeMeasuringModule, CounterType.HeatEnergyNodeThermoResistance, CounterType.CurrentTransformer}.Contains(mainCounter.Type))
            {
                expanderIndicationsGrid.Visibility = Visibility.Collapsed;
                expanderIndicationsGraphs.Visibility = Visibility.Collapsed;
            }
            else
            {

                //Устанавливаем начальные значения дат
                dateStart.SelectedDate = DateTime.Now.AddYears(-1).AddMonths(-(DateTime.Now.Month - 1));
                dateEnd.SelectedDate = DateTime.Now.AddMonths(12 - DateTime.Now.Month);

                //Создаем график показаний
                graph = new IndicationNodeGraph(mainCounter);
                graph.Height = 200;
                graph.SetBinding(WidthProperty, "{Binding Width, ElementName = graphExpanderPanel}");
                graphExpanderPanel.Children.Add(graph);
                UpdateGridData(false);
            }

            //Регестрируем эвенты изменения текста
            counterExtraType.TextChanged += textbox_TextChanged;
            counterModel.TextChanged += textbox_TextChanged;
            counterNumber.TextChanged += textbox_TextChanged;

            measuringModule1ExtraType.TextChanged += textbox_TextChanged;
            measuringModule2ExtraType.TextChanged += textbox_TextChanged;
            measuringModule1Model.TextChanged += textbox_TextChanged;
            measuringModule2Model.TextChanged += textbox_TextChanged;
            measuringModule1Number.TextChanged += textbox_TextChanged;
            measuringModule2Number.TextChanged += textbox_TextChanged;

            thermoRes1ExtraType.TextChanged += textbox_TextChanged;
            thermoRes2ExtraType.TextChanged += textbox_TextChanged;
            thermoRes1Model.TextChanged += textbox_TextChanged;
            thermoRes2Model.TextChanged += textbox_TextChanged;
            thermoRes1Number.TextChanged += textbox_TextChanged;
            thermoRes2Number.TextChanged += textbox_TextChanged;
            
            btnImgSaveChanges.IsEnabled = false;
        }

        public void UpdateGridData(bool updateGraph = true)
        {
            //MessageBox.Show("TRIGGER");
            if (dateStart.SelectedDate != null && dateEnd.SelectedDate != null)
            {
                if (mainCounter.Type == CounterType.ColdWaterNode || 
                    mainCounter.Type == CounterType.HeatEnergyNode || 
                    mainCounter.Type == CounterType.ElectricNode ||
                    (mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.Mechanical) )
                {
                    //Расход
                    var table = mainCounter.GetConsumptionTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value);
                    ConsumptionTable = YearIndication.GetObservableCollection(table);
                    consumptionGrid.ItemsSource = ConsumptionTable;

                    //Показания
                    var table2 = mainCounter.GetIndicationsTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value);
                    IndicationsTable = YearIndication.GetObservableCollection(table2);
                    indicationsGrid.ItemsSource = IndicationsTable;

                }
                else if(mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.MechanicalСirc)
                {
                    //Расход
                    var table = mainCounter.GetConsumptionTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value, true);
                    ConsumptionTable = YearIndication.GetObservableCollection(table);
                    consumptionGrid.ItemsSource = ConsumptionTable;

                    //Показания подача
                    var table2 = mainCounter.GetIndicationsTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value);
                    IndicationsTable = YearIndication.GetObservableCollection(table2);
                    indicationsGrid.ItemsSource = IndicationsTable;

                    //Показания обратка
                    var secondaryCounter = associatedDevices.Single(c => /*c.AssociatedDeviceID == mainCounter.ID &&*/ c.Type == CounterType.HotWaterNode && c.SubType == CounterSubType.Return);
                    var table3 = secondaryCounter.GetIndicationsTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value);
                    IndicationsTable2 = YearIndication.GetObservableCollection(table3);
                    indicationsGrid2.ItemsSource = IndicationsTable2;

                    expanderIndicationsGrid3.Visibility = Visibility.Visible;
                }
                else if (mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.Digital)
                {
                    if (indicationsType.SelectedIndex == 0)
                    {
                        //Расход тепло
                        var table = mainCounter.GetConsumptionTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value);
                        ConsumptionTable = YearIndication.GetObservableCollection(table);
                        consumptionGrid.ItemsSource = ConsumptionTable;

                        //Показания тепло
                        var table2 = mainCounter.GetIndicationsTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value);
                        IndicationsTable = YearIndication.GetObservableCollection(table2);
                        indicationsGrid.ItemsSource = IndicationsTable;
                    }
                    else if (indicationsType.SelectedIndex == 1)
                    {
                        var measuringModule = associatedDevices.Single(c => /*c.AssociatedDeviceID == mainCounter.ID &&*/ c.Type == CounterType.HeatEnergyNodeMeasuringModule);

                        //Расход вода
                        var table = measuringModule.GetConsumptionTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value);
                        ConsumptionTable = YearIndication.GetObservableCollection(table);
                        consumptionGrid.ItemsSource = ConsumptionTable;

                        //Показания вода
                        var table2 = measuringModule.GetIndicationsTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value);
                        IndicationsTable = YearIndication.GetObservableCollection(table2);
                        indicationsGrid.ItemsSource = IndicationsTable;
                    }
                    expanderIndicationsGrid3.Visibility = Visibility.Collapsed;
                }
                else if (mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.DigitalСirc)
                {
                    if (indicationsType.SelectedIndex == 0)
                    {
                        //Расход тепло
                        var table = mainCounter.GetConsumptionTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value);
                        ConsumptionTable = YearIndication.GetObservableCollection(table);
                        consumptionGrid.ItemsSource = ConsumptionTable;

                        //Показания тепло
                        var table2 = mainCounter.GetIndicationsTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value);
                        IndicationsTable = YearIndication.GetObservableCollection(table2);
                        indicationsGrid.ItemsSource = IndicationsTable;

                        expanderIndicationsGrid3.Visibility = Visibility.Collapsed;
                    }
                    else if (indicationsType.SelectedIndex == 1)
                    {
                        var measuringModuleSuply  = associatedDevices.Single(c => /*c.AssociatedDeviceID == mainCounter.ID &&*/ c.Type == CounterType.HeatEnergyNodeMeasuringModule && c.SubType == CounterSubType.Suply);
                        var measuringModuleReturn = associatedDevices.Single(c => /*c.AssociatedDeviceID == mainCounter.ID &&*/ c.Type == CounterType.HeatEnergyNodeMeasuringModule && c.SubType == CounterSubType.Return);


                        //Расход
                        var table = mainCounter.GetConsumptionTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value, true);
                        ConsumptionTable = YearIndication.GetObservableCollection(table);
                        consumptionGrid.ItemsSource = ConsumptionTable;

                        //Показания подача
                        var table2 = measuringModuleSuply.GetIndicationsTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value);
                        IndicationsTable = YearIndication.GetObservableCollection(table2);
                        indicationsGrid.ItemsSource = IndicationsTable;

                        //Показания обратка
                        var table3 = measuringModuleReturn.GetIndicationsTableForPeriod(dateStart.SelectedDate.Value, dateEnd.SelectedDate.Value);
                        IndicationsTable2 = YearIndication.GetObservableCollection(table3);
                        indicationsGrid2.ItemsSource = IndicationsTable2;

                        expanderIndicationsGrid3.Visibility = Visibility.Visible;
                    }

                }
            }

            consumptionGrid.FrozenColumnCount = 1;
            indicationsGrid.FrozenColumnCount = 1;
            //indicationsGrid.AutoGeneratingColumn += IndicationsGrid_AutoGeneratingColumn;
            //indicationsGrid2.AutoGeneratingColumn += IndicationsGrid_AutoGeneratingColumn;

            if (updateGraph) graph?.UpdateValues();
        }

        private void expanderIndicationsGrid_Collapsed(object sender, RoutedEventArgs e)
        {
            ((Expander)sender).VerticalAlignment = VerticalAlignment.Top;
            ((Expander)sender).MinHeight = 23;
            if (WindowState == WindowState.Normal) SizeToContent = SizeToContent.Height;
            if (sender.Equals(expanderIndicationsGrid))
            {
                indicationsGridRow1.Height = new GridLength(1, GridUnitType.Auto);
            }
            else if (sender.Equals(expanderIndicationsGrid2))
            {
                indicationsGridRow2.Height = new GridLength(1, GridUnitType.Auto);
            }
            else if (sender.Equals(expanderIndicationsGrid3))
            {
                indicationsGridRow3.Height = new GridLength(1, GridUnitType.Auto);
            }
        }

        private void expanderIndicationsGrid_Expanded(object sender, RoutedEventArgs e)
        {
            ((Expander)sender).VerticalAlignment = VerticalAlignment.Stretch;
            ((Expander)sender).MinHeight = 187;
            if(WindowState == WindowState.Normal) SizeToContent = SizeToContent.Height;
            //if (sender.Equals(expanderIndicationsGrid))
            //{
            //    indicationsGridRow1.Height = new GridLength(1, GridUnitType.Star);
            //}
            //else if (sender.Equals(expanderIndicationsGrid2))
            //{
            //    indicationsGridRow2.Height = new GridLength(1, GridUnitType.Star);
            //}
            //else if (sender.Equals(expanderIndicationsGrid3))
            //{
            //    indicationsGridRow3.Height = new GridLength(1, GridUnitType.Star);
            //}
        }

        #region DataTablesPeriod
        private void dateStart_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            dateStart.SelectedDateChanged -= dateStart_SelectedDateChanged;
            if (dateEnd.SelectedDate != null && e.RemovedItems.Count > 0)
            {
                if ((DateTime)e.AddedItems[0] > dateEnd.SelectedDate)
                {
                    dateStart.SelectedDate = (DateTime)e.RemovedItems[0];
                    MessageBox.Show("Дата начала периода не может быть позже даты конца периода", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    UpdateGridData(false);
                }
            }
            dateStart.SelectedDateChanged += dateStart_SelectedDateChanged;
        }

        private void dateEnd_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            dateEnd.SelectedDateChanged -= dateEnd_SelectedDateChanged;
            if (dateStart.SelectedDate != null && e.RemovedItems.Count > 0)
            {
                if ((DateTime)e.AddedItems[0] < dateStart.SelectedDate)
                {
                    dateEnd.SelectedDate = (DateTime)e.RemovedItems[0];
                    MessageBox.Show("Дата конца периода не может быть раньше даты начала периода", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    UpdateGridData(false);
                }
            }
            dateEnd.SelectedDateChanged += dateEnd_SelectedDateChanged;
        }
        #endregion

        //private void btnAdd_Click(object sender, RoutedEventArgs e)
        //{
        //    var addIndicationsWindow = new AddNodeIndications(mainCounter);
        //    addIndicationsWindow.Owner = this;
        //    addIndicationsWindow.ShowDialog();
        //}

        private void Window_StateChanged(object sender, EventArgs e)
        {
            if(WindowState == WindowState.Normal) SizeToContent = SizeToContent.Height;
        }

        #region DatePickerHandlers

        private void measuringModule1CheckDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count == 0) return;
            ((DatePicker)sender).SelectedDateChanged -= measuringModule1CheckDate_SelectedDateChanged;
            if (MessageBox.Show("Установить новую дату следующей поверки?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
            {
                ((DatePicker)sender).SelectedDate = (DateTime) e.RemovedItems[0];
                ((DatePicker)sender).SelectedDateChanged += measuringModule1CheckDate_SelectedDateChanged;
                return;
            }
            var measuringModule = ContextFactory.Instance.ConsumptionCounters.Where(c => c.AssociatedDeviceID == mainCounter.ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule).ToArray()[0];
            measuringModule.NextCheckDate = (DateTime)e.AddedItems[0];
            ContextFactory.Instance.SaveChanges();
            ((DatePicker)sender).SelectedDateChanged += measuringModule1CheckDate_SelectedDateChanged;
            ((FacilityCard)Owner).UpdateData();
            App.AppMainWindow.SetUpdateData();
        }

        private void measuringModule2CheckDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count == 0) return;
            ((DatePicker)sender).SelectedDateChanged -= measuringModule2CheckDate_SelectedDateChanged;
            if (MessageBox.Show("Установить новую дату следующей поверки?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
            {
                ((DatePicker)sender).SelectedDate = (DateTime)e.RemovedItems[0];
                ((DatePicker)sender).SelectedDateChanged += measuringModule2CheckDate_SelectedDateChanged;
                return;
            }
            var measuringModule = ContextFactory.Instance.ConsumptionCounters.Where(c => c.AssociatedDeviceID == mainCounter.ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule).ToArray()[1];
            measuringModule.NextCheckDate = (DateTime)e.AddedItems[0];
            ContextFactory.Instance.SaveChanges();
            ((DatePicker)sender).SelectedDateChanged += measuringModule2CheckDate_SelectedDateChanged;
            ((FacilityCard)Owner).UpdateData();
            App.AppMainWindow.SetUpdateData();
        }

        private void thermoRes1CheckDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count == 0) return;
            ((DatePicker)sender).SelectedDateChanged -= thermoRes1CheckDate_SelectedDateChanged;
            if (MessageBox.Show("Установить новую дату следующей поверки?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
            {
                ((DatePicker)sender).SelectedDate = (DateTime)e.RemovedItems[0];
                ((DatePicker)sender).SelectedDateChanged += thermoRes1CheckDate_SelectedDateChanged;
                return;
            }
            var thermoresistance = ContextFactory.Instance.ConsumptionCounters.Where(c => c.AssociatedDeviceID == mainCounter.ID && c.Type == CounterType.HeatEnergyNodeThermoResistance).ToArray()[0];
            thermoresistance.NextCheckDate = (DateTime)e.AddedItems[0];
            ContextFactory.Instance.SaveChanges();
            ((DatePicker)sender).SelectedDateChanged += thermoRes1CheckDate_SelectedDateChanged;
            ((FacilityCard)Owner).UpdateData();
            App.AppMainWindow.SetUpdateData();
        }

        private void thermoRes2CheckDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count == 0) return;
            ((DatePicker)sender).SelectedDateChanged -= thermoRes2CheckDate_SelectedDateChanged;
            if (MessageBox.Show("Установить новую дату следующей поверки?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
            {
                ((DatePicker)sender).SelectedDate = (DateTime)e.RemovedItems[0];
                ((DatePicker)sender).SelectedDateChanged += thermoRes2CheckDate_SelectedDateChanged;
                return;
            }
            var thermoresistance = ContextFactory.Instance.ConsumptionCounters.Where(c => c.AssociatedDeviceID == mainCounter.ID && c.Type == CounterType.HeatEnergyNodeThermoResistance).ToArray()[1];
            thermoresistance.NextCheckDate = (DateTime)e.AddedItems[0];
            ContextFactory.Instance.SaveChanges();
            ((DatePicker)sender).SelectedDateChanged += thermoRes2CheckDate_SelectedDateChanged;
            ((FacilityCard)Owner).UpdateData();
            App.AppMainWindow.SetUpdateData();
        }

        private void counterCheckDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count == 0) return;
            ((DatePicker)sender).SelectedDateChanged -= counterCheckDate_SelectedDateChanged;
            if (MessageBox.Show("Установить новую дату следующей поверки?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
            {
                ((DatePicker)sender).SelectedDate = (DateTime)e.RemovedItems[0];
                ((DatePicker)sender).SelectedDateChanged += counterCheckDate_SelectedDateChanged;
                return;
            }
            mainCounter.NextCheckDate = (DateTime)e.AddedItems[0];
            ContextFactory.Instance.SaveChanges();
            ((DatePicker)sender).SelectedDateChanged += counterCheckDate_SelectedDateChanged;
            ((FacilityCard)Owner).UpdateData();
            App.AppMainWindow.SetUpdateData();
        }

        private void SecondaryCounterCheckDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count == 0) return;
            ((DatePicker)sender).SelectedDateChanged -= counterCheckDate_SelectedDateChanged;
            if (MessageBox.Show("Установить новую дату следующей поверки?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
            {
                ((DatePicker)sender).SelectedDate = (DateTime)e.RemovedItems[0];
                ((DatePicker)sender).SelectedDateChanged += SecondaryCounterCheckDate_SelectedDateChanged;
                return;
            }
            var secondaryCounter = associatedDevices.Single(c => /*c.AssociatedDeviceID == mainCounter.ID &&*/ c.Type == CounterType.HotWaterNode && c.SubType == CounterSubType.Return);
            secondaryCounter.NextCheckDate = (DateTime)e.AddedItems[0];
            ContextFactory.Instance.SaveChanges();
            ((DatePicker)sender).SelectedDateChanged += SecondaryCounterCheckDate_SelectedDateChanged;
            ((FacilityCard)Owner).UpdateData();
            App.AppMainWindow.SetUpdateData();
        }

        #endregion

        private void textbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (btnImgSaveChanges.IsEnabled) return;
            btnImgSaveChanges.IsEnabled = true;
        }

        private void btnImgSaveChanges_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Сохранить изменения?", "Сохранение", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No) return;
            else btnImgSaveChanges.IsEnabled = false;
            mainCounter.ExtraType = counterExtraType.Text;
            mainCounter.Model = counterModel.Text;
            mainCounter.Number = counterNumber.Text;
            if (mainCounter.Type == CounterType.HeatEnergyNode || (mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.Digital))
            {
                var measuringModules = associatedDevices.Where(c => /*c.AssociatedDeviceID == mainCounter.ID &&*/ c.Type == CounterType.HeatEnergyNodeMeasuringModule).ToArray();
                var thermoresistances = associatedDevices.Where(c => /*c.AssociatedDeviceID == mainCounter.ID &&*/ c.Type == CounterType.HeatEnergyNodeThermoResistance).ToArray();

                measuringModules[0].ExtraType = measuringModule1ExtraType.Text;
                measuringModules[0].Model = measuringModule1Model.Text;
                measuringModules[0].Number = measuringModule1Number.Text;

                if (!(mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.Digital))
                {
                    measuringModules[1].ExtraType = measuringModule2ExtraType.Text;
                    measuringModules[1].Model = measuringModule2Model.Text;
                    measuringModules[1].Number = measuringModule2Number.Text;
                }

                //Если есть термосопротивления сохраняем их
                if (thermoresistances.Any())
                {
                    thermoresistances[0].ExtraType = thermoRes1ExtraType.Text;
                    thermoresistances[0].Model = thermoRes1Model.Text;
                    thermoresistances[0].Number = thermoRes1Number.Text;

                    if (!(mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.Digital))
                    {
                        thermoresistances[1].ExtraType = thermoRes2ExtraType.Text;
                        thermoresistances[1].Model = thermoRes2Model.Text;
                        thermoresistances[1].Number = thermoRes2Number.Text;
                    }
                }
            }
            //сохранить изменения второго счетчика при мех циркуляции
            if (mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.MechanicalСirc)
            {
                var returnCounter = associatedDevices.Single(c => /*c.AssociatedDeviceID == mainCounter.ID &&*/ c.Type == CounterType.HotWaterNode && c.SubType == CounterSubType.Return);
                returnCounter.ExtraType = SecondaryCounterExtraType.Text;
                returnCounter.Model = SecondaryCounterModel.Text;
                returnCounter.Number = SecondaryCounterNumber.Text;
            }
            CurrentTransformator?.Save();


            /*
             ТАБЛИЦЫ
             */
            if (IndicationsTableChanged)
            {
                ConsumptionCounter counter = mainCounter;
                if (indicationsType.SelectedIndex == 1)
                {
                    if (mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.Digital)
                    {
                        counter = associatedDevices.Single(c => c.AssociatedDeviceID == mainCounter.ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule);
                    }
                    else if (mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.DigitalСirc)
                    {
                        counter = associatedDevices.Single(c => c.AssociatedDeviceID == mainCounter.ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule && c.SubType == CounterSubType.Suply);
                    }
                }

                foreach (YearIndication row in IndicationsTable)
                {
                    var values = row.Values;
                    var year = int.Parse(values[0]);
                    for (var month = 1; month <= 12; month++)
                    {
                        var indication = counter.Indications.SingleOrDefault(i => i.Date.Year == year && i.Date.Month == month);
                        double value;
                        if (double.TryParse(values[month].Replace('.', ','), out value))
                        {
                            if (indication == null)
                            {
                                indication = new Indication(value, counter, new DateTime(year, month, 1));
                                ContextFactory.Instance.Indications.Add(indication);
                            }
                            else
                            {
                                indication.Value = value;
                            }
                        }
                    }
                }
            }
            if (IndicationsTable2Changed)
            {
                ConsumptionCounter counter = null;
                if (mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.MechanicalСirc)
                {
                    counter = associatedDevices.Single(c => c.AssociatedDeviceID == mainCounter.ID && c.Type == CounterType.HotWaterNode && c.SubType == CounterSubType.Return);
                }
                else if (mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.DigitalСirc)
                {
                    counter = associatedDevices.Single(c => c.AssociatedDeviceID == mainCounter.ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule && c.SubType == CounterSubType.Return);
                }
                foreach (YearIndication row in IndicationsTable2)
                {
                    var values = row.Values;
                    var year = int.Parse(values[0]);
                    for (var month = 1; month <= 12; month++)
                    {
                        var indication = counter.Indications.SingleOrDefault(i => i.Date.Year == year && i.Date.Month == month);
                        double value;
                        if (double.TryParse(values[month].Replace('.', ','), out value))
                        {
                            if (indication == null)
                            {
                                indication = new Indication(value, counter, new DateTime(year, month, 1));
                                ContextFactory.Instance.Indications.Add(indication);
                            }
                            else
                            {
                                indication.Value = value;
                            }
                        }
                    }
                }
            }
            ContextFactory.Instance.ChangeTracker.DetectChanges();
            ContextFactory.Instance.SaveChanges();
            ((FacilityCard)Owner).UpdateData();
            UpdateGridData();
        }

        private void indicationsGrid2_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            btnImgSaveChanges.IsEnabled = true;
            IndicationsTableChanged = true;
            indicationsGrid.CellEditEnding -= indicationsGrid2_CellEditEnding;
            DataGrid grid = (DataGrid)sender;
            grid.CommitEdit(DataGridEditingUnit.Row, true);
            indicationsGrid.CellEditEnding += indicationsGrid2_CellEditEnding;
        }
        
        private void indicationsGrid3_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            btnImgSaveChanges.IsEnabled = true;
            IndicationsTable2Changed = true;
            indicationsGrid2.CellEditEnding -= indicationsGrid3_CellEditEnding;
            DataGrid grid = (DataGrid)sender;
            grid.CommitEdit(DataGridEditingUnit.Row, true);
            indicationsGrid2.CellEditEnding += indicationsGrid3_CellEditEnding;
        }

        public class YearIndication : INotifyPropertyChanged
        {
            YearIndication(DataRow row)
            {
                year = row[0].ToString();
                jan  = row[1].ToString();
                feb  = row[2].ToString();
                mar  = row[3].ToString();
                apr  = row[4].ToString();
                may  = row[5].ToString();
                jun  = row[6].ToString();
                jul  = row[7].ToString();
                aug  = row[8].ToString();
                sep  = row[9].ToString();
                oct  = row[10].ToString();
                nov  = row[11].ToString();
                dec  = row[12].ToString();
            }
            
            [Display(Name = "Год")]
            public string year { get; set; }

            private string _jan;
            [Display(Name = "Январь")]
            public string jan {
                get => this._jan;
                set
                {
                    if (value != this._jan)
                    {
                        this._jan = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            private string _feb;
            [Display(Name = "Февраль")]
            public string feb
            {
                get => this._feb; set
                {
                    if (value != this._feb)
                    {
                        this._feb = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            private string _mar;

            [Display(Name = "Март")]
            public string mar
            {
                get => this._mar; set
                {
                    if (value != this._mar)
                    {
                        this._mar = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            private string _apr;

            [Display(Name = "Апрель")]
            public string apr
            {
                get => this._apr; set
                {
                    if (value != this._apr)
                    {
                        this._apr = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            private string _may;

            [Display(Name = "Май")]
            public string may
            {
                get => this._may; set
                {
                    if (value != this._may)
                    {
                        this._may = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            private string _jun;

            [Display(Name = "Июнь")]
            public string jun
            {
                get => this._jun; set
                {
                    if (value != this._jun)
                    {
                        this._jun = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            private string _jul;

            [Display(Name = "Июль")]
            public string jul
            {
                get => this._jul; set
                {
                    if (value != this._jul)
                    {
                        this._jul = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            private string _aug;

            [Display(Name = "Август")]
            public string aug
            {
                get => this._aug; set
                {
                    if (value != this._aug)
                    {
                        this._aug = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            private string _sep;

            [Display(Name = "Сентябрь")]
            public string sep
            {
                get => this._sep; set
                {
                    if (value != this._sep)
                    {
                        this._sep = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            private string _oct;

            [Display(Name = "Октябрь")]
            public string oct
            {
                get => this._oct; set
                {
                    if (value != this._oct)
                    {
                        this._oct = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            private string _nov;

            [Display(Name = "Ноябрь")]
            public string nov
            {
                get => this._nov; set
                {
                    if (value != this._nov)
                    {
                        this._nov = value;
                        NotifyPropertyChanged();
                    }
                }
            }

            private string _dec;

            [Display(Name = "Декабрь")]
            public string dec
            {
                get => this._dec; set
                {
                    if (value != this._dec)
                    {
                        this._dec = value;
                        NotifyPropertyChanged();
                    }
                }
            }
            
            public string[] Values => new[] {year, jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec };

            public static ObservableCollection<YearIndication> GetObservableCollection(DataTable data)
            {
                ObservableCollection<YearIndication> _result = new ObservableCollection<YearIndication>();
                foreach (DataRow row in data.Rows)
                {
                    _result.Add(new YearIndication(row));
                }
                return _result;
            }

            public override string ToString()
            {
                StringBuilder output = new StringBuilder();
                for (int i = 1; i <= 12; i++)
                {
                    output.Append(Values[i] + " ");
                }
                return output.ToString();
            }

            public event PropertyChangedEventHandler PropertyChanged;

            // This method is called by the Set accessor of each property.
            // The CallerMemberName attribute that is applied to the optional propertyName
            // parameter causes the property name of the caller to be substituted as an argument.
            private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
                //MessageBox.Show("trigger change");
            }
        }

        private void indicationsGrid2_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "Values")
            {
                e.Cancel = true;
                //e.Column.Visibility = Visibility.Collapsed;
                return;
            }
            if (e.PropertyName == "year")
            {
                e.Column.IsReadOnly = true;
                e.Column.Width = DataGridLength.Auto;
            }
            else
            {
                e.Column.MinWidth = 70;
                ((DataGridTextColumn)e.Column).Binding = new Binding(e.PropertyName) { StringFormat = "{0:F}" };
            }
            var attr = (DisplayAttribute)typeof(YearIndication).GetProperty(e.PropertyName).GetCustomAttributes(typeof(DisplayAttribute)).FirstOrDefault();
            e.Column.Header = attr?.Name;
        }

        private void indicationsType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.DigitalСirc)
            {
                if (indicationsType.SelectedIndex == 0)
                {
                    expanderIndicationsGrid2.Header = "Показания";
                    expanderIndicationsGrid3.Visibility = Visibility.Collapsed;
                    if (graph != null)
                        graph.WaterCirculation = false;
                }
                else if (indicationsType.SelectedIndex == 1)
                {
                    expanderIndicationsGrid2.Header = "Показания подача";
                    expanderIndicationsGrid3.Visibility = Visibility.Visible;
                    if (graph != null)
                        graph.WaterCirculation = true;
                }
            }
            if (mainCounter.Type == CounterType.HotWaterNode && mainCounter.SubType == CounterSubType.Digital)
            {
                if (indicationsType.SelectedIndex == 0)
                {
                    if(graph != null)
                        graph.Counter = mainCounter;
                }
                else if (indicationsType.SelectedIndex == 1)
                {
                    if (graph != null)
                        graph.Counter = associatedDevices.Single(c => c.AssociatedDeviceID == mainCounter.ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule);
                }
            }
            UpdateGridData();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (btnImgSaveChanges.IsEnabled) btnImgSaveChanges_Click(null, null);
        }
    }
}
