﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using AccountingСounters.CustomControls;
using AccountingСounters.Database;
using AccountingСounters.Enums;
using AccountingСounters.Migrations;
using AccountingСounters.Models;
using Data.Enums;

namespace AccountingСounters.Interface.Node
{
    /// <summary>
    /// Interaction logic for AddNewCounterExpanded.xaml
    /// </summary>
    public partial class AddNewCounterExpanded : Window
    {
        public static List<string> counterTypeName = new List<string>()
        {
            CounterType.ElectricNode.GetDescription(),
            CounterType.ColdWaterNode.GetDescription(),
            CounterType.HotWaterNode.GetDescription(),
            CounterType.HeatEnergyNode.GetDescription()
        };

        public static List<string> hotWaterCounterSubTypeName = new List<string>()
        {
            CounterSubType.Mechanical.GetDescription(),
            CounterSubType.MechanicalСirc.GetDescription(),
            CounterSubType.Digital.GetDescription(),
            CounterSubType.DigitalСirc.GetDescription()
        };
        //public static Dictionary<int, Tuple<CounterType, CounterSubType>> counterRealTypeDictionary = new Dictionary<int, Tuple<CounterType, CounterSubType>>()
        //{
        //    {0, new Tuple<CounterType, CounterSubType>(CounterType.ColdWaterNode, CounterSubType.Default) },
        //    {1, new Tuple<CounterType, CounterSubType>(CounterType.HotWaterNode, CounterSubType.Mechanical) },
        //    {2, new Tuple<CounterType, CounterSubType>(CounterType.HotWaterNode, CounterSubType.Digital) },
        //    {3, new Tuple<CounterType, CounterSubType>(CounterType.ElectricNode, CounterSubType.Default) },
        //    {4, new Tuple<CounterType, CounterSubType>(CounterType.HeatEnergyNode, CounterSubType.Default) },
        //};
        public static Dictionary<int, CounterType> counterTypeDictionary = new Dictionary<int, CounterType>()
        {
            {0, CounterType.ElectricNode },
            {1, CounterType.ColdWaterNode },
            {2, CounterType.HotWaterNode },
            {3, CounterType.HeatEnergyNode }
        };

        public static Dictionary<int, CounterSubType> counterSubTypeDictionary = new Dictionary<int, CounterSubType>()
        {
            {0, CounterSubType.Mechanical },
            {1, CounterSubType.MechanicalСirc },
            {2, CounterSubType.Digital },
            {3, CounterSubType.DigitalСirc }
        };

        private bool add;
        private readonly CurrentTransformatorInfo currentTransformator = new CurrentTransformatorInfo();
        public AddNewCounterExpanded()
        {
            InitializeComponent();
            counterType.ItemsSource = counterTypeName;
            counterSubType.ItemsSource = hotWaterCounterSubTypeName;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!add && MessageBox.Show("Все не сохраненные изменения будут утеряны.\n Закрыть?", "Отмена", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                add = false;
                e.Cancel = true;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (counterExtraType.Text == "" ||
                counterModel.Text == "" ||
                counterNumber.Text == "" ||
                counterCheckDate.SelectedDate == null ||

                ((counterType.SelectedIndex == 2 && counterSubType.SelectedIndex == 1) &&
                 (secondaryCounterExtraType.Text == "" ||
                  secondaryCounterModel.Text == "" ||
                  secondaryCounterNumber.Text == "" ||
                  secondaryCounterCheckDate.SelectedDate == null)) ||

                ((counterType.SelectedIndex == 3 ||
                  counterType.SelectedIndex == 2 && (counterSubType.SelectedIndex == 2 || counterSubType.SelectedIndex == 3)) &&
                (measuringModule1ExtraType.Text == "" ||
                 measuringModule1Model.Text == "" ||
                 measuringModule1Number.Text == "" ||
                 measuringModule1CheckDate.SelectedDate == null ||

                 (counterType.SelectedIndex == 3 ||
                  counterType.SelectedIndex == 2 && counterSubType.SelectedIndex == 3) &&
                 (measuringModule2ExtraType.Text == "" ||
                 measuringModule2Model.Text == "" ||
                 measuringModule2Number.Text == "" ||
                 measuringModule2CheckDate.SelectedDate == null) ||

                 (currentTransCheckbox.IsChecked.Value &&
                 (thermoRes1ExtraType.Text == "" ||
                 thermoRes1Model.Text == "" ||
                 thermoRes1Number.Text == "" ||
                 thermoRes1CheckDate.SelectedDate == null ||

                 (counterType.SelectedIndex == 3 ||
                  counterType.SelectedIndex == 2 && counterSubType.SelectedIndex == 3) &&
                 (thermoRes2ExtraType.Text == "" ||
                 thermoRes2Model.Text == "" ||
                 thermoRes2Number.Text == "" ||
                 thermoRes2CheckDate.SelectedDate == null))))) ||
                 (currentTransCheckbox.IsChecked.Value && (string)counterType.SelectedItem == CounterType.ElectricNode.GetDescription() &&
                 !currentTransformator.IsInputValid))
            {
                MessageBox.Show("Заполнены не все поля", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            add = true;
            MaintainedFacility facility = (MaintainedFacility)App.AppMainWindow.mainDataGrid.SelectedItem;
            ConsumptionCounter newCounter = null;
            try
            {
                    newCounter = new ConsumptionCounter(
                    counterTypeDictionary[counterType.SelectedIndex],
                    counterType.SelectedIndex == 2 ? counterSubTypeDictionary[counterSubType.SelectedIndex] : CounterSubType.Default,
                    counterExtraType.Text,
                    counterModel.Text,
                    counterNumber.Text,
                    counterCheckDate.SelectedDate.Value,
                    facility.ID);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            ContextFactory.Instance.ConsumptionCounters.Add(newCounter);
            //ContextFactory.Instance.SaveChanges();

            //Если узел учета тепла или электронный узел учета горячей воды
            if (counterType.SelectedIndex == 3 || //Узел учета тепла
                counterType.SelectedIndex == 2 && (counterSubType.SelectedIndex == 2 || counterSubType.SelectedIndex == 3)) //Электронный узел учета воды
            {
                ConsumptionCounter newMeasuringModule1 = null;
                ConsumptionCounter newMeasuringModule2 = null;
                ConsumptionCounter newThermores1 = null;
                ConsumptionCounter newThermores2 = null;
                try
                {
                        newMeasuringModule1 = new ConsumptionCounter(
                            CounterType.HeatEnergyNodeMeasuringModule,
                            CounterSubType.Default,
                            measuringModule1ExtraType.Text,
                            measuringModule1Model.Text,
                            measuringModule1Number.Text,
                            (DateTime)measuringModule1CheckDate.SelectedDate,
                            facility.ID,
                            newCounter);
                        if (counterType.SelectedIndex == 3 || //Узел учета тепла
                            counterType.SelectedIndex == 2 && counterSubType.SelectedIndex == 3)
                        {
                            newMeasuringModule2 = new ConsumptionCounter(
                                CounterType.HeatEnergyNodeMeasuringModule,
                                CounterSubType.Default,
                                measuringModule2ExtraType.Text,
                                measuringModule2Model.Text,
                                measuringModule2Number.Text,
                                (DateTime)measuringModule2CheckDate.SelectedDate,
                                facility.ID,
                                newCounter);
                        }
                        if (currentTransCheckbox.IsChecked.Value)
                        {
                            newThermores1 = new ConsumptionCounter(
                                CounterType.HeatEnergyNodeThermoResistance,
                                CounterSubType.Default,
                                thermoRes1ExtraType.Text,
                                thermoRes1Model.Text,
                                thermoRes1Number.Text,
                                (DateTime) thermoRes1CheckDate.SelectedDate,
                                facility.ID,
                                newCounter);
                            if (counterType.SelectedIndex == 3 || //Узел учета тепла
                                counterType.SelectedIndex == 2 && counterSubType.SelectedIndex == 3)
                            {
                                newThermores2 = new ConsumptionCounter(
                                    CounterType.HeatEnergyNodeThermoResistance,
                                    CounterSubType.Default,
                                    thermoRes2ExtraType.Text,
                                    thermoRes2Model.Text,
                                    thermoRes2Number.Text,
                                    (DateTime) thermoRes2CheckDate.SelectedDate,
                                    facility.ID,
                                    newCounter);
                            }
                        }
                    if (counterType.SelectedIndex == 2) //Если узел учета горячей воды то установить типы изм модулей на подачу и обратку
                    {
                        newMeasuringModule1.SubType = CounterSubType.Suply;
                        if (counterSubType.SelectedIndex == 3) // Если тип счетчика циркуляция
                        {
                            newMeasuringModule2.SubType = CounterSubType.Return;
                        }
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                ContextFactory.Instance.ConsumptionCounters.Add(newMeasuringModule1);
                if (currentTransCheckbox.IsChecked.Value)
                {
                    ContextFactory.Instance.ConsumptionCounters.Add(newThermores1);
                }

                if (counterType.SelectedIndex == 3 || //Узел учета тепла
                    counterType.SelectedIndex == 2 && counterSubType.SelectedIndex == 3)
                {
                    ContextFactory.Instance.ConsumptionCounters.Add(newMeasuringModule2);
                    if (currentTransCheckbox.IsChecked.Value)
                    {
                        ContextFactory.Instance.ConsumptionCounters.Add(newThermores2);
                    }
                }
            }
            //Если механический счетчик с циркуляцией
            else if (counterType.SelectedIndex == 2 && counterSubType.SelectedIndex == 1) //Мех. узел цирк воды
            {
                ConsumptionCounter newMeasuringModule1 = null;
                //ConsumptionCounter newMeasuringModule2 = null;
                try
                {
                    newMeasuringModule1 = new ConsumptionCounter(
                        counterTypeDictionary[counterType.SelectedIndex],
                        CounterSubType.Return,
                        secondaryCounterExtraType.Text,
                        secondaryCounterModel.Text,
                        secondaryCounterNumber.Text,
                        (DateTime)secondaryCounterCheckDate.SelectedDate,
                        facility.ID,
                        newCounter);
                    //newMeasuringModule2 = new ConsumptionCounter(
                    //    CounterType.HeatEnergyNodeMeasuringModule,
                    //    CounterSubType.Return,
                    //    "SYSTEM_RETURN",
                    //    "SYSTEM_RETURN",
                    //    "SYSTEM_RETURN",
                    //    DateTime.MaxValue,
                    //    facility.ID,
                    //    newCounter);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                ContextFactory.Instance.ConsumptionCounters.Add(newMeasuringModule1);
                //ContextFactory.Instance.ConsumptionCounters.Add(newMeasuringModule2);
            }
            //Если счетчик электричества и есть трансформатор
            else if (currentTransCheckbox.IsChecked.Value && (string)counterType.SelectedItem == CounterType.ElectricNode.GetDescription())
            {
                ConsumptionCounter transformator = null;
                try
                {
                    transformator = new ConsumptionCounter(
                        CounterType.CurrentTransformer,
                        CounterSubType.Default,
                        currentTransformator.Type,
                        currentTransformator.Model,
                        currentTransformator.Numbers,
                        (DateTime)currentTransformator.CheckDate,
                        facility.ID,
                        newCounter);
                    transformator.ExtraValue = currentTransformator.TransformationCoefficient.ToString(CultureInfo.InvariantCulture);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                ContextFactory.Instance.ConsumptionCounters.Add(transformator);
            }
            ContextFactory.Instance.SaveChanges();
            ((Facility.FacilityCard)this.Owner).UpdateData();
            App.AppMainWindow.SetUpdateData();
            Close();
        }

        private void counterType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            secondaryCounterGroupBox.Visibility = Visibility.Collapsed;
            if (counterType.SelectedIndex == 3) //Счетчик тепла
            {
                measuringModulesContainer.Visibility = Visibility.Visible;
                //thermoresModulesContainer.Visibility = Visibility.Visible;
                subTypePanel.Visibility = Visibility.Collapsed;
                mainCounterGroupBox.Header = "Вычислитель";
            }
            else if(counterType.SelectedIndex == 2) // Счетчики ГВС
            {
                subTypePanel.Visibility = Visibility.Visible;
                if (counterSubType.SelectedIndex > 1) //Электронные
                {
                    measuringModulesContainer.Visibility = Visibility.Visible;
                    //thermoresModulesContainer.Visibility = Visibility.Visible;
                    mainCounterGroupBox.Header = "Вычислитель";
                }
                else//Механические
                {
                    measuringModulesContainer.Visibility = Visibility.Collapsed;
                    thermoresModulesContainer.Visibility = Visibility.Collapsed;
                    mainCounterGroupBox.Header = "Счетчик";
                    if (counterSubType.SelectedIndex == 1) //Циркуляция
                    {
                        mainCounterGroupBox.Header = "Счетчик подача";
                        secondaryCounterGroupBox.Visibility = Visibility.Visible;
                    }
                }
            }
            else
            {
                measuringModulesContainer.Visibility = Visibility.Collapsed;
                thermoresModulesContainer.Visibility = Visibility.Collapsed;
                subTypePanel.Visibility = Visibility.Collapsed;
                mainCounterGroupBox.Header = "Счетчик";
            }

            if (counterType.SelectedIndex == 2 && counterSubType.SelectedIndex == 2) //убрать второй модуль и сопротивление
            {
                secondModule.Width = new GridLength(0);
                secondThermores.Width = new GridLength(0);
            }
            else
            {
                secondModule.Width = new GridLength(1, GridUnitType.Star);
                secondThermores.Width = new GridLength(1, GridUnitType.Star);
            }

            currentTransCheckbox.IsChecked = false;
            if (counterType.SelectedIndex == 0) //Electric Node
            {
                currentTransCheckbox.Visibility = Visibility.Visible;
                currentTransCheckbox.Content = "Трансформатор тока";
                if (currentTransCheckbox.IsChecked.Value)
                {
                    if (!innerStack.Children.Contains(currentTransformator)) innerStack.Children.Add(currentTransformator);
                }
            }
            else if (counterType.SelectedIndex == 3 || (counterType.SelectedIndex == 2 && counterSubType.SelectedIndex > 1))
            {
                currentTransCheckbox.Visibility = Visibility.Visible;
                currentTransCheckbox.Content = "Термосопротивление";
            }
            else
            {
                if (innerStack.Children.Contains(currentTransformator)) innerStack.Children.Remove(currentTransformator);
                currentTransCheckbox.Visibility = Visibility.Collapsed;
            }
        }

        private void currentTransCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            if (counterType.SelectedIndex == 0) //Electric Node
            {
                if (!innerStack.Children.Contains(currentTransformator)) innerStack.Children.Add(currentTransformator);
            }
            else if (counterType.SelectedIndex == 3) //Счетчик тепла
            {
                thermoresModulesContainer.Visibility = Visibility.Visible;
            }
            else if (counterType.SelectedIndex == 2) // Счетчики ГВС
            {
                if (counterSubType.SelectedIndex > 1) //Электронные
                {
                    thermoresModulesContainer.Visibility = Visibility.Visible;
                }
            }
        }

        private void currentTransCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (counterType.SelectedIndex == 0) //Electric Node
            {
                if (innerStack.Children.Contains(currentTransformator)) innerStack.Children.Remove(currentTransformator);
            }
            else if (counterType.SelectedIndex == 3) //Счетчик тепла
            {
                thermoresModulesContainer.Visibility = Visibility.Collapsed;
            }
            else if (counterType.SelectedIndex == 2) // Счетчики ГВС
            {
                if (counterSubType.SelectedIndex > 1) //Электронные
                {
                    thermoresModulesContainer.Visibility = Visibility.Collapsed;
                }
            }
        }

        #region SameTextMeasuringModule
        private void measuringModule1ExtraType_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sameTypeModelDateCheckbox.IsChecked != null && (bool) sameTypeModelDateCheckbox.IsChecked)
            {
                measuringModule2ExtraType.Text = measuringModule1ExtraType.Text;
            }
        }

        private void measuringModule1Model_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sameTypeModelDateCheckbox.IsChecked != null && (bool)sameTypeModelDateCheckbox.IsChecked)
            {
                measuringModule2Model.Text = measuringModule1Model.Text;
            }
        }

        private void measuringModule1CheckDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sameTypeModelDateCheckbox.IsChecked != null && (bool)sameTypeModelDateCheckbox.IsChecked)
            {
                measuringModule2CheckDate.SelectedDate = measuringModule1CheckDate.SelectedDate;
            }
        }
        
        private void CheckBox_Check_Uncheck(object sender, RoutedEventArgs e)
        {
            var isChecked = ((CheckBox)sender).IsChecked;
            if (isChecked != null && (bool)isChecked)
            {
                measuringModule2ExtraType.IsReadOnly = true;
                measuringModule2Model.IsReadOnly = true;
                measuringModule2CheckDate.IsEnabled = false;
            }
            else if (isChecked != null && !(bool)isChecked)
            {
                measuringModule2ExtraType.IsReadOnly = false;
                measuringModule2Model.IsReadOnly = false;
                measuringModule2CheckDate.IsEnabled = true;
            }
        }
        #endregion

        #region SameTextThermores

        private void thermoRes1ExtraType_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sameTypeModelDateCheckbox.IsChecked != null && (bool)sameTypeModelDateCheckbox.IsChecked)
            {
                thermoRes2ExtraType.Text = thermoRes1ExtraType.Text;
            }
        }

        private void thermoRes1Model_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sameTypeModelDateCheckbox.IsChecked != null && (bool)sameTypeModelDateCheckbox.IsChecked)
            {
                thermoRes2Model.Text = thermoRes1Model.Text;
            }
        }

        private void thermoRes1CheckDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sameTypeModelDateCheckbox.IsChecked != null && (bool)sameTypeModelDateCheckbox.IsChecked)
            {
                thermoRes2CheckDate.SelectedDate = thermoRes1CheckDate.SelectedDate;
            }
        }

        private void CheckBox2_Check_Uncheck(object sender, RoutedEventArgs e)
        {
            var isChecked = ((CheckBox)sender).IsChecked;
            if (isChecked != null && (bool)isChecked)
            {
                thermoRes2ExtraType.IsReadOnly = true;
                thermoRes2Model.IsReadOnly = true;
                thermoRes2CheckDate.IsEnabled = false;
            }
            else if (isChecked != null && !(bool)isChecked)
            {
                thermoRes2ExtraType.IsReadOnly = false;
                thermoRes2Model.IsReadOnly = false;
                thermoRes2CheckDate.IsEnabled = true;
            }
        }
        #endregion
    }
}
