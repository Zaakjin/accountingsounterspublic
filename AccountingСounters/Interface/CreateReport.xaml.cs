﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AccountingСounters.Database;
using AccountingСounters.Enums;
using AccountingСounters.Helpers.XLHelpers;
using AccountingСounters.Models;
using ClosedXML.Excel;

namespace AccountingСounters.Interface
{
    /// <summary>
    /// Interaction logic for CreateReport.xaml
    /// </summary>
    public partial class CreateReport : Window
    {
        public CreateReport()
        {
            InitializeComponent();
        }

        private static readonly List<string> ReportTypeList = new List<string>{"Электроэнергия",
                                                                      "Холодная вода",
                                                                      "Горячая вода и отопление"};

        private void Window_Initialized(object sender, EventArgs e)
        {
            ReportTypeCombobox.ItemsSource = ReportTypeList;
            ReportTypeCombobox.SelectedIndex = 0;
            monthPicker.SelectedDate = DateTime.Now;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = $"Отчет - {ReportTypeCombobox.SelectedItem} - {monthPicker.Text}".Replace("\"", ""); // Default file name
            dlg.DefaultExt = ".xlsx";               // Default file extension
            dlg.Filter = "Таблица (.xlsx)|*.xlsx";  // Filter files by extension
            dlg.AddExtension = true;

            // Show save file dialog box
            bool? result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;
                FileInfo file = new FileInfo(dlg.FileName);
                try
                {
                    using (FileStream fileStream = file.Open(FileMode.OpenOrCreate,
                        FileAccess.ReadWrite, FileShare.None))
                    {
                        createExcelReport(fileStream);
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Файл занят другой программой", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //Console.WriteLine(exception);
                    //throw;
                }

            }
        }

        private void createExcelReport(Stream fileName)
        {

            var workbook = new XLWorkbook(XLEventTracking.Disabled);
            var worksheet = workbook.AddWorksheet("Отчет");

            worksheet.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
            //worksheet.Style.Font.FontName = "Cambira";
            worksheet.Style.Font.FontFamilyNumbering = XLFontFamilyNumberingValues.Roman;
            worksheet.Style.Fill.BackgroundColor = XLColor.White;

            //worksheet.Column(1).Style.Font.FontColor = XLColor.White;
            var cell = worksheet.Cell(2, 1);
            //var range = worksheet.Range("A2:G2");
            var range = worksheet.Range(2,1,2,7);
            //Загловок
            range.Merge();
            cell.Value = "Показания по счетчикам - " + ReportTypeCombobox.SelectedItem;
            cell.Style.Font.FontSize = 14;

            //Число
            cell = worksheet.Cell(3, 2);
            cell.Value = "ЗА:";
            cell.Style.Font.FontSize = 12;
            cell.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

            cell = worksheet.Cell(3, 3);
            cell.Value = monthPicker.Text;
            cell.Style.Font.FontSize = 12;
            cell.Style.NumberFormat.NumberFormatId = 14;


            var Facilities = ContextFactory.Instance.MaintainedFacilities.Include(f => f.Counters).ToList();
            int i = 5;
            string startCol;
            string endCol;
            switch (ReportTypeCombobox.SelectedIndex)
            {
                case 0: //Электроэнергия
                    //Шапка таблицы
                    startCol = "B";
                    endCol   = "F";
                    range = worksheet.Range($@"{startCol}4:{endCol}4");
                    range.Style.Fill.BackgroundColor = XLColor.Gray;
                    range.SetAllBorders(XLBorderStyleValues.Thin);
                    range.Style.Font.FontColor = XLColor.White;
                    //range.Cell(1, 1).Value = "ТИП УЗЛА УЧЕТА";

                    worksheet.Cell(4, 2).Value = "НОМЕР CЧЕТЧИКА";
                    worksheet.Cell(4, 3).Value = "ПРЕД. ПОКАЗАНИЯ";
                    worksheet.Cell(4, 4).Value = "ТЕК. ПОКАЗАНИЯ";
                    worksheet.Cell(4, 5).Value = "КОЭФ.";
                    worksheet.Cell(4, 6).Value = "РАСХОД";

                    range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    range.Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                    //Узлы учета
                    //int i = 5;
                    foreach (var Facility in Facilities)
                    {
                        var FacilityEnergyCounters = ContextFactory.Instance.ConsumptionCounters.Where(c => c.MaintainedFacilityID == Facility.ID && c.Type == CounterType.ElectricNode);
                        if(!FacilityEnergyCounters.Any()) continue;
                        range = worksheet.Range($@"{startCol}{i}:{endCol}{i}");
                        range.Merge();
                        worksheet.Cell(i, 2).Value = $@"{Facility.Name} - {Facility.Address}";
                        worksheet.Cell(i, 2).Style.Fill.BackgroundColor = XLColor.LightGray;
                        range.SetAllBorders(XLBorderStyleValues.Thin);
                        i++;
                        foreach (ConsumptionCounter counter in FacilityEnergyCounters)
                        {
                            range = worksheet.Range($@"{startCol}{i}:{endCol}{i}");
                            range.SetAllBorders(XLBorderStyleValues.Thin);
                            worksheet.Cell(i, 2).Value = counter.Number;
                            worksheet.Cell(i, 3).Value = counter.IndicationsForDate(monthPicker.SelectedDate.Value.AddMonths(-1));
                            worksheet.Cell(i, 4).Value = counter.IndicationsForDate(monthPicker.SelectedDate.Value);
                            var transformer = ContextFactory.Instance.ConsumptionCounters.SingleOrDefault(c => c.AssociatedDeviceID == counter.ID && c.Type == CounterType.CurrentTransformer);
                            worksheet.Cell(i, 5).Value = transformer == null ? 1d : double.Parse(transformer.ExtraValue);
                            worksheet.Cell(i, 6).Value = counter.ConsumtionForDate(monthPicker.SelectedDate.Value);
                            i++;
                        }
                    }
                    break;
                case 1: //Холодная вода
                    //Шапка таблицы
                    startCol = "B";
                    endCol = "E";
                    range = worksheet.Range($@"{startCol}4:{endCol}4");
                    range.Style.Fill.BackgroundColor = XLColor.Gray;
                    range.SetAllBorders(XLBorderStyleValues.Thin);
                    range.Style.Font.FontColor = XLColor.White;

                    worksheet.Cell(4, 2).Value = "НОМЕР CЧЕТЧИКА";
                    worksheet.Cell(4, 3).Value = "ПРЕД. ПОКАЗАНИЯ";
                    worksheet.Cell(4, 4).Value = "ТЕК. ПОКАЗАНИЯ";
                    worksheet.Cell(4, 5).Value = "РАСХОД";

                    range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    range.Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    //Узлы учета
                    //int i = 5;
                    foreach (var Facility in Facilities)
                    {
                        var FacilityColdWaterCounters = ContextFactory.Instance.ConsumptionCounters.Where(c => c.MaintainedFacilityID == Facility.ID && c.Type == CounterType.ColdWaterNode);
                        if (!FacilityColdWaterCounters.Any()) continue;
                        range = worksheet.Range($@"{startCol}{i}:{endCol}{i}");
                        range.Merge();
                        worksheet.Cell(i, 2).Value = $@"{Facility.Name} - {Facility.Address}";
                        worksheet.Cell(i, 2).Style.Fill.BackgroundColor = XLColor.LightGray;
                        range.SetAllBorders(XLBorderStyleValues.Thin);
                        i++;
                        //var FacilityCounters = ContextFactory.Instance.ConsumptionCounters.Where(c => c.MaintainedFacilityID == Facility.ID);
                        foreach (ConsumptionCounter counter in FacilityColdWaterCounters)
                        {
                            range = worksheet.Range($@"{startCol}{i}:{endCol}{i}");
                            range.SetAllBorders(XLBorderStyleValues.Thin);
                            worksheet.Cell(i, 2).Value = counter.Number;
                            worksheet.Cell(i, 3).Value = counter.IndicationsForDate(monthPicker.SelectedDate.Value.AddMonths(-1));
                            worksheet.Cell(i, 4).Value = counter.IndicationsForDate(monthPicker.SelectedDate.Value);
                            worksheet.Cell(i, 5).Value = counter.ConsumtionForDate(monthPicker.SelectedDate.Value);
                            i++;
                        }
                    }
                    break;
                case 2: //Горячая вода и отопление
                    //Шапка таблицы
                    startCol = "B";
                    endCol = "E";
                    range = worksheet.Range($@"{startCol}4:{endCol}4");
                    range.Style.Fill.BackgroundColor = XLColor.Gray;
                    range.SetAllBorders(XLBorderStyleValues.Thin);
                    range.Style.Font.FontColor = XLColor.White;

                    worksheet.Cell(4, 2).Value = "НОМЕР CЧЕТЧИКА";
                    worksheet.Cell(4, 3).Value = "ПРЕД. ПОКАЗАНИЯ";
                    worksheet.Cell(4, 4).Value = "ТЕК. ПОКАЗАНИЯ";
                    worksheet.Cell(4, 5).Value = "РАСХОД";

                    range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    range.Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    //Узлы учета
                    //int i = 5;
                    foreach (var Facility in Facilities)
                    {
                        var FacilityHotWaterCounters = ContextFactory.Instance.ConsumptionCounters.Where(c => c.MaintainedFacilityID == Facility.ID && (c.Type == CounterType.HeatEnergyNode || c.Type == CounterType.HotWaterNode)).OrderBy(c => c.Type).ThenBy(c=> c.SubType);
                        if (!FacilityHotWaterCounters.Any()) continue;
                        range = worksheet.Range($@"{startCol}{i}:{endCol}{i}");
                        range.Merge();
                        worksheet.Cell(i, 2).Value = $@"{Facility.Name} - {Facility.Address}";
                        worksheet.Cell(i, 2).Style.Fill.BackgroundColor = XLColor.LightGray;
                        range.SetAllBorders(XLBorderStyleValues.Thin);
                        i++;
                        var j = i;
                        foreach (ConsumptionCounter counter in FacilityHotWaterCounters)
                        {
                            range = worksheet.Range($@"{startCol}{i}:{endCol}{i}");
                            range.SetAllBorders(XLBorderStyleValues.Thin);

                            if (counter.Type == CounterType.HeatEnergyNode || (counter.Type == CounterType.HotWaterNode && counter.SubType == CounterSubType.Mechanical))
                            {
                                worksheet.Cell(i, 2).Value = counter.FullTypeShortNoExtra + " - № " + counter.Number;
                                worksheet.Cell(i, 3).Value = counter.IndicationsForDate(monthPicker.SelectedDate.Value.AddMonths(-1));
                                worksheet.Cell(i, 4).Value = counter.IndicationsForDate(monthPicker.SelectedDate.Value);
                                worksheet.Cell(i, 5).Value = counter.ConsumtionForDate(monthPicker.SelectedDate.Value);
                            }
                            else if (counter.Type == CounterType.HotWaterNode)
                            {
                                if (counter.SubType == CounterSubType.MechanicalСirc)
                                {
                                    worksheet.Cell(i, 2).Value = counter.FullTypeShortNoExtra + " - № " + counter.Number;
                                    i++;
                                    worksheet.Cell(i, 2).SetValue("Подача:");
                                    worksheet.Cell(i, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                                    worksheet.Cell(i, 3).Value = counter.IndicationsForDate(monthPicker.SelectedDate.Value.AddMonths(-1));
                                    worksheet.Cell(i, 4).Value = counter.IndicationsForDate(monthPicker.SelectedDate.Value);
                                    worksheet.Cell(i, 5).Value = counter.ConsumtionForDate(monthPicker.SelectedDate.Value);
                                    i++;
                                    var returnCounter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.AssociatedDeviceID == counter.ID && c.Type == CounterType.HotWaterNode && c.SubType == CounterSubType.Return);
                                    worksheet.Cell(i, 2).SetValue("Обратка:");
                                    worksheet.Cell(i, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                                    worksheet.Cell(i, 3).Value = returnCounter.IndicationsForDate(monthPicker.SelectedDate.Value.AddMonths(-1));
                                    worksheet.Cell(i, 4).Value = returnCounter.IndicationsForDate(monthPicker.SelectedDate.Value);
                                    worksheet.Cell(i, 5).Value = returnCounter.ConsumtionForDate(monthPicker.SelectedDate.Value);
                                    i++;
                                    worksheet.Cell(i, 2).SetValue("Итого:");
                                    worksheet.Cell(i, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                                    worksheet.Cell(i, 5).Value = counter.ConsumtionForDate(monthPicker.SelectedDate.Value, true);

                                }
                                else if (counter.SubType == CounterSubType.Digital)
                                {
                                    worksheet.Cell(i, 2).Value = counter.FullTypeShortNoExtra + " - № " + counter.Number;
                                    i++;
                                    worksheet.Cell(i, 2).SetValue("Тепло:");
                                    worksheet.Cell(i, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                                    worksheet.Cell(i, 3).Value = counter.IndicationsForDate(monthPicker.SelectedDate.Value.AddMonths(-1));
                                    worksheet.Cell(i, 4).Value = counter.IndicationsForDate(monthPicker.SelectedDate.Value);
                                    worksheet.Cell(i, 5).Value = counter.ConsumtionForDate(monthPicker.SelectedDate.Value);
                                    i++;
                                    var waterCounter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.AssociatedDeviceID == counter.ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule);
                                    worksheet.Cell(i, 2).SetValue("Вода:");
                                    worksheet.Cell(i, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                                    worksheet.Cell(i, 3).Value = waterCounter.IndicationsForDate(monthPicker.SelectedDate.Value.AddMonths(-1));
                                    worksheet.Cell(i, 4).Value = waterCounter.IndicationsForDate(monthPicker.SelectedDate.Value);
                                    worksheet.Cell(i, 5).Value = waterCounter.ConsumtionForDate(monthPicker.SelectedDate.Value);
                                }
                                else if (counter.SubType == CounterSubType.DigitalСirc)
                                {
                                    worksheet.Cell(i, 2).Value = counter.FullTypeShortNoExtra + " - № " + counter.Number;
                                    i++;
                                    worksheet.Cell(i, 2).SetValue("Тепло:");
                                    worksheet.Cell(i, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                                    worksheet.Cell(i, 3).Value = counter.IndicationsForDate(monthPicker.SelectedDate.Value.AddMonths(-1));
                                    worksheet.Cell(i, 4).Value = counter.IndicationsForDate(monthPicker.SelectedDate.Value);
                                    worksheet.Cell(i, 5).Value = counter.ConsumtionForDate(monthPicker.SelectedDate.Value);
                                    i++;
                                    var suplyCounter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.AssociatedDeviceID == counter.ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule && c.SubType == CounterSubType.Suply);
                                    worksheet.Cell(i, 2).SetValue("Подача:");
                                    worksheet.Cell(i, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                                    worksheet.Cell(i, 3).Value = suplyCounter.IndicationsForDate(monthPicker.SelectedDate.Value.AddMonths(-1));
                                    worksheet.Cell(i, 4).Value = suplyCounter.IndicationsForDate(monthPicker.SelectedDate.Value);
                                    worksheet.Cell(i, 5).Value = suplyCounter.ConsumtionForDate(monthPicker.SelectedDate.Value);
                                    i++;
                                    var returnCounter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.AssociatedDeviceID == counter.ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule && c.SubType == CounterSubType.Return);
                                    worksheet.Cell(i, 2).SetValue("Обратка:");
                                    worksheet.Cell(i, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                                    worksheet.Cell(i, 3).Value = returnCounter.IndicationsForDate(monthPicker.SelectedDate.Value.AddMonths(-1));
                                    worksheet.Cell(i, 4).Value = returnCounter.IndicationsForDate(monthPicker.SelectedDate.Value);
                                    worksheet.Cell(i, 5).Value = returnCounter.ConsumtionForDate(monthPicker.SelectedDate.Value);
                                    i++;
                                    worksheet.Cell(i, 2).SetValue("Итого:");
                                    worksheet.Cell(i, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                                    worksheet.Cell(i, 5).Value = counter.ConsumtionForDate(monthPicker.SelectedDate.Value, true);
                                }
                            }
                            i++;
                        }
                        i--;
                        worksheet.Range(j, 2, i, 5).SetAllBorders(XLBorderStyleValues.Thin);
                    }
                    break;
            }

            //Окончательное форматирование
            worksheet.Rows(1, worksheet.LastRowUsed().RowNumber()).Height = 20;
            worksheet.Rows(1, worksheet.LastRowUsed().RowNumber()).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
            //worksheet.Rows(1, worksheet.LastRowUsed().Cell(1).Address.RowNumber).Height = 20;
            //worksheet.Rows(1, worksheet.LastRowUsed().Cell(1).Address.RowNumber).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
            worksheet.Column(1).Width = 1;
            worksheet.Columns(2, 6).AdjustToContents();

            workbook.SaveAs(fileName);
        }
    }
}
