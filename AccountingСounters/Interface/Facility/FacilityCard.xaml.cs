﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using AccountingСounters.CustomControls;
using AccountingСounters.Database;
using AccountingСounters.Enums;
using AccountingСounters.Models;
using ClosedXML.Excel;

namespace AccountingСounters.Interface.Facility
{
    /// <summary>
    /// Interaction logic for FacilityCard.xaml
    /// </summary>
    public partial class FacilityCard : Window
    {
        public static string EXCEL_FILLER = "//";
        private List<ConsumptionCounter> Data { get; set; }
        private MaintainedFacility Facility { get; set; }
        private readonly List<CounterType> checkList = new List<CounterType> { CounterType.ColdWaterNode, CounterType.HotWaterNode, CounterType.ElectricNode, CounterType.HeatEnergyNode };
        private readonly List<CounterSubType> checkList2 = new List<CounterSubType> { CounterSubType.Default, CounterSubType.Mechanical, CounterSubType.MechanicalСirc, CounterSubType.Digital, CounterSubType.DigitalСirc };

        public void UpdateData()
        {
            Data = ContextFactory.Instance.ConsumptionCounters.Where(c => c.MaintainedFacilityID == Facility.ID && checkList.Contains(c.Type) && checkList2.Contains(c.SubType)).ToList();
            countersGrid.ItemsSource = Data;
            //Data = ContextFactory.Instance.ConsumptionCounters.Where(c => c.MaintainedFacilityID == Facility.ID && new List<CounterType>{CounterType.ColdWaterNode, CounterType.HotWaterNode, CounterType.ElectricNode, CounterType.HeatEnergyNode}.Contains(c.Type)).ToList();
            //Data = ContextFactory.Instance.ConsumptionCounters.Where(c => c.MaintainedFacilityID == ((MaintainedFacility)App.AppMainWindow.mainDataGrid.SelectedItem).ID).ToList();
        }
        public FacilityCard()
        {
            InitializeComponent();
            Facility = (MaintainedFacility)App.AppMainWindow.mainDataGrid.SelectedItem;
            UpdateData();
            //Таблица узлов учета
            countersGrid.ItemsSource = Data;

            //Установка данных в поля
            facilityName.Text = Facility.Name;
            facilityAdress.Text = Facility.Address;

            sentryPhone.Text = Facility.SentryPhone;

            string[] manager = Facility.ManagerName.Split(' ');
            managerLastName.Text = manager[0];
            managerName.Text = manager[1];
            managerSurname.Text = manager[2];
            managerPhone.Text = Facility.ManagerPhone;

            string[] liable = Facility.IndicationsLiableName.Split(' ');
            liableLastName.Text = liable[0];
            liableName.Text = liable[1];
            liableSurName.Text = liable[2];
            liablePhone.Text = Facility.IndicationsLiablePhone;


            //Колонка тип
            counterTypeColumn.Binding = new Binding(nameof(ConsumptionCounter.FullTypeShort));

            //Колонка модель
            counterModelColumn.Binding = new Binding(nameof(ConsumptionCounter.Model));

            //Колонка номер
            counterNumberColumn.Binding = new Binding(nameof(ConsumptionCounter.Number));

            //Колонка дата очередной поверки
            var binding = new Binding(nameof(ConsumptionCounter.NextCheckDate));
            binding.Converter = new NumberToStringConverterExtension();
            counterCheckDateColumn.Binding = binding;
            
            //Кнопка карточка узла учета c картинкой
            Binding facilityCardBinding = new Binding();
            facilityCardBinding.Converter = new MainWindow.IsDataGridItemSelected();
            facilityCardBinding.Source = countersGrid;
            facilityCardBinding.Path = new PropertyPath("SelectedIndex");
            btnImgCounterInfo.SetBinding(IsEnabledProperty, facilityCardBinding);
            btnImgDeleteCounter.SetBinding(IsEnabledProperty, facilityCardBinding);

            //Инициализируем обработчики
            facilityName.TextChanged += textbox_TextChanged;
            facilityAdress.TextChanged += textbox_TextChanged;

            sentryPhone.TextChanged += textbox_TextChanged;

            managerLastName.TextChanged += textbox_TextChanged;
            managerName.TextChanged += textbox_TextChanged;
            managerSurname.TextChanged += textbox_TextChanged;
            managerPhone.TextChanged += textbox_TextChanged;

            liableLastName.TextChanged += textbox_TextChanged;
            liableName.TextChanged += textbox_TextChanged;
            liableSurName.TextChanged += textbox_TextChanged;
            liablePhone.TextChanged += textbox_TextChanged;

        }

        private void btnAddNewCounter_Click(object sender, RoutedEventArgs e)
        {
            var newAddNewCounterWnd = new Node.AddNewCounterExpanded();
            newAddNewCounterWnd.Owner = this;
            newAddNewCounterWnd.ShowDialog();
        }

        public class NumberToStringConverterExtension : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return ((DateTime)value).ToShortDateString();
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return value;
            }
        }

        private void countersGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(countersGrid.SelectedItem == null) return;
            Node.AccountingNodeInfo nodeInfoWnd = new Node.AccountingNodeInfo();
            nodeInfoWnd.Owner = this;
            nodeInfoWnd.PostInit();
            nodeInfoWnd.ShowDialog();
        }

        private void btnCounterInfo_Click(object sender, RoutedEventArgs e)
        {
            Node.AccountingNodeInfo nodeInfoWnd = new Node.AccountingNodeInfo();
            nodeInfoWnd.Owner = this;
            nodeInfoWnd.PostInit();
            nodeInfoWnd.ShowDialog();
        }

        private void btnImgInputFacilityIndications_Click(object sender, RoutedEventArgs e)
        {
            AddAllCountersIndications allCountersIndications = new AddAllCountersIndications(Facility);
            allCountersIndications.Owner = this;
            allCountersIndications.ShowDialog();
        }

        private void btnImgFacilityConsGraphs_Click(object sender, RoutedEventArgs e)
        {
            if (graphScroll.Content == null)
            {
                var Graphs = new FacilityIndicationNodeGraph(Facility);
                Graphs.SetBinding(WidthProperty, "{Binding Width, ElementName=graphScroll}");
                graphScroll.Content = Graphs;

                if (WindowState != WindowState.Maximized) this.SizeToContent = SizeToContent.Width;
            }
            if (graphPanel.Visibility == Visibility.Visible)
            {
                graphPanel.Visibility = Visibility.Collapsed;
                dataColumn.Width = new GridLength(1, GridUnitType.Star);
                graphColumn.Width = new GridLength(1, GridUnitType.Auto);
                this.MinWidth = 550;
                if (WindowState != WindowState.Maximized) Width = 475;
            }
            else
            {
                graphPanel.Visibility = Visibility.Visible;
                dataColumn.Width = new GridLength(460, GridUnitType.Pixel);
                graphColumn.Width = new GridLength(1, GridUnitType.Star);
                this.MinWidth = 850;
            }
        }

        private void btnImgSaveChanges_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Сохранить изменения?", "Сохранение", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No) return;
            btnImgSaveChanges.IsEnabled = false;

            Facility.Name = facilityName.Text;
            Facility.Address = facilityAdress.Text;
            Facility.ManagerName = $"{managerLastName.Text} {managerName.Text} {managerSurname.Text}";
            Facility.ManagerPhone = managerPhone.Text;
            Facility.IndicationsLiableName = $"{liableLastName.Text} {liableName.Text} {liableSurName.Text}";
            Facility.IndicationsLiablePhone = liablePhone.Text;
            Facility.SentryPhone = sentryPhone.Text;

            ContextFactory.Instance.SaveChanges();
            App.AppMainWindow.SetUpdateData();
            //ContextFactory.Instance.SaveChanges();
        }

        private void textbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(btnImgSaveChanges.IsEnabled) return;
            btnImgSaveChanges.IsEnabled = true;
        }

        private void btnSaveIndicationsForm_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = $"Форма подачи показаний - {Facility.Name}".Replace("\"",""); // Default file name
            dlg.DefaultExt = ".xlsx";               // Default file extension
            dlg.Filter = "Таблица (.xlsx)|*.xlsx";  // Filter files by extension
            dlg.AddExtension = true;

            // Show save file dialog box
            bool? result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;
                FileInfo file = new FileInfo(dlg.FileName);
                try
                {
                    using (FileStream fileStream = file.Open(FileMode.OpenOrCreate,
                        FileAccess.ReadWrite, FileShare.None))
                    {
                        createExcelReport(fileStream);
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Файл занят другой программой", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //Console.WriteLine(exception);
                    //throw;
                }

            }
        }

        private void createExcelReport(Stream fileName)
        {
            var workbook = new XLWorkbook(XLEventTracking.Disabled);
            var worksheet = workbook.AddWorksheet("Indications");

            worksheet.Protect("132")    // On this sheet we will only allow:
                .SetFormatCells()       // Cell Formatting
                .SetInsertColumns()     // Inserting Columns
                .SetDeleteColumns()     // Deleting Columns
                .SetDeleteRows();       // Deleting Rows

            worksheet.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
            //worksheet.Style.Font.FontName = "Cambira";
            worksheet.Style.Font.FontFamilyNumbering = XLFontFamilyNumberingValues.Roman;
            worksheet.Style.Fill.BackgroundColor = XLColor.White;

            worksheet.Column(1).Style.Font.FontColor = XLColor.White;
            var cell = worksheet.Cell(1, 2);
            var range = worksheet.Range("B1:G1");
            //Загловок
            range.Merge();
            cell.Value = "ПОКАЗАНИЯ ПРИБОРОВ УЧЕТА";
            cell.Style.Font.FontSize = 22;

            //Название объекта
            worksheet.Cell(1, 1).Value = Facility.ID;
            cell = worksheet.Cell(2, 2);
            cell.Value = Facility.Name;
            cell.Style.Font.FontSize = 11;

            //Адрес объекта
            cell = worksheet.Cell(3, 2);
            cell.Value = Facility.Address;
            cell.Style.Font.FontSize = 10;

            //Число
            cell = worksheet.Cell(4, 2);
            cell.Value = "ЗА:";
            cell.Style.Font.FontSize = 12;
            cell.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

            cell = worksheet.Cell(4, 3);
            cell.Value = DateTime.Now.Date;
            cell.Style.Font.FontSize = 12;
            cell.Style.NumberFormat.NumberFormatId = 14;
            cell.Style.Fill.BackgroundColor = XLColor.LightGray;
            cell.Style.Protection.SetLocked(false);
            cell.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            cell.Style.Border.OutsideBorderColor = XLColor.Gray;

            //Шапка таблицы
            range = worksheet.Range("B6:D6");
            range.Style.Fill.BackgroundColor = XLColor.Gray;
            range.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            range.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            range.Style.Font.FontColor = XLColor.White;
            range.Cell(1, 1).Value = "ТИП УЗЛА УЧЕТА";
            range.Cell(1, 2).Value = "НОМЕР";
            range.Cell(1, 3).Value = "ПОКАЗАНИЯ";
            range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            range.Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);

            //Узлы учета
            int i = 7;
            foreach (var counter in Data)
            {
                var j = i;
                if (counter.Type == CounterType.ElectricNode || counter.Type == CounterType.ColdWaterNode || counter.Type == CounterType.HeatEnergyNode || (counter.Type == CounterType.HotWaterNode && counter.SubType == CounterSubType.Mechanical))
                {
                    worksheet.Cell(i, 1).Value = counter.ID;
                    worksheet.Cell(i, 2).SetValue(counter.FullTypeShort);
                    worksheet.Cell(i, 3).SetValue(counter.Number);
                    worksheet.Cell(i, 4).Style.Protection.SetLocked(false);
                    worksheet.Cell(i, 4).Style.Fill.BackgroundColor = XLColor.LightGray;
                }
                else if (counter.Type == CounterType.HotWaterNode && counter.SubType == CounterSubType.MechanicalСirc)
                {
                    worksheet.Cell(i, 1).Value = EXCEL_FILLER;
                    worksheet.Cell(i, 2).SetValue(counter.FullTypeShort);
                    worksheet.Cell(i, 3).SetValue(counter.Number);
                    i++;
                    worksheet.Cell(i, 1).Value = counter.ID;
                    worksheet.Cell(i, 3).SetValue("Подача:");
                    worksheet.Cell(i, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    worksheet.Cell(i, 4).Style.Protection.SetLocked(false);
                    worksheet.Cell(i, 4).Style.Fill.BackgroundColor = XLColor.LightGray;
                    i++;
                    var returnCounter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.AssociatedDeviceID == counter.ID && c.Type == CounterType.HotWaterNode && c.SubType == CounterSubType.Return);
                    worksheet.Cell(i, 1).Value = returnCounter.ID;
                    worksheet.Cell(i, 3).SetValue("Обратка:");
                    worksheet.Cell(i, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    worksheet.Cell(i, 4).Style.Protection.SetLocked(false);
                    worksheet.Cell(i, 4).Style.Fill.BackgroundColor = XLColor.LightGray;

                }
                else if (counter.Type == CounterType.HotWaterNode && counter.SubType == CounterSubType.Digital)
                {
                    worksheet.Cell(i, 1).Value = EXCEL_FILLER;
                    worksheet.Cell(i, 2).SetValue(counter.FullTypeShort);
                    worksheet.Cell(i, 3).SetValue(counter.Number);
                    i++;
                    worksheet.Cell(i, 1).Value = counter.ID;
                    worksheet.Cell(i, 3).SetValue("Тепло:");
                    worksheet.Cell(i, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    worksheet.Cell(i, 4).Style.Protection.SetLocked(false);
                    worksheet.Cell(i, 4).Style.Fill.BackgroundColor = XLColor.LightGray;
                    i++;
                    var returnCounter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.AssociatedDeviceID == counter.ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule);
                    worksheet.Cell(i, 1).Value = returnCounter.ID;
                    worksheet.Cell(i, 3).SetValue("Вода:");
                    worksheet.Cell(i, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    worksheet.Cell(i, 4).Style.Protection.SetLocked(false);
                    worksheet.Cell(i, 4).Style.Fill.BackgroundColor = XLColor.LightGray;
                }
                else if (counter.Type == CounterType.HotWaterNode && counter.SubType == CounterSubType.DigitalСirc)
                {
                    worksheet.Cell(i, 1).Value = EXCEL_FILLER;
                    worksheet.Cell(i, 2).SetValue(counter.FullTypeShort);
                    worksheet.Cell(i, 3).SetValue(counter.Number);
                    i++;
                    worksheet.Cell(i, 1).Value = counter.ID;
                    worksheet.Cell(i, 3).SetValue("Тепло:");
                    worksheet.Cell(i, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    worksheet.Cell(i, 4).Style.Protection.SetLocked(false);
                    worksheet.Cell(i, 4).Style.Fill.BackgroundColor = XLColor.LightGray;
                    i++;
                    var returnCounter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.AssociatedDeviceID == counter.ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule && c.SubType == CounterSubType.Suply);
                    worksheet.Cell(i, 1).Value = returnCounter.ID;
                    worksheet.Cell(i, 3).SetValue("Подача:");
                    worksheet.Cell(i, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    worksheet.Cell(i, 4).Style.Protection.SetLocked(false);
                    worksheet.Cell(i, 4).Style.Fill.BackgroundColor = XLColor.LightGray;
                    i++;
                    returnCounter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.AssociatedDeviceID == counter.ID && c.Type == CounterType.HeatEnergyNodeMeasuringModule && c.SubType == CounterSubType.Return);
                    worksheet.Cell(i, 1).Value = returnCounter.ID;
                    worksheet.Cell(i, 3).SetValue("Обратка:");
                    worksheet.Cell(i, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    worksheet.Cell(i, 4).Style.Protection.SetLocked(false);
                    worksheet.Cell(i, 4).Style.Fill.BackgroundColor = XLColor.LightGray;
                }
                //var rng = worksheet.Range(i, 2, i, 4);
                var rng = worksheet.Range(j, 2, i, 4);
                rng.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rng.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                i++;
            }
            
            //Окончательное форматирование
            worksheet.Rows(1, worksheet.LastRowUsed().Cell(1).Address.RowNumber).Height = 21;
            worksheet.Rows(1, worksheet.LastRowUsed().Cell(1).Address.RowNumber).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
            worksheet.Column(1).Width = 1;
            worksheet.Column(2).AdjustToContents();
            worksheet.Columns(3, 4).Width = 25;
            workbook.SaveAs(fileName);
        }

        private void btnImgDeleteCounter_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show($"Удалить {((ConsumptionCounter)countersGrid.SelectedItem).FullType.ToLower()} №{((ConsumptionCounter)countersGrid.SelectedItem).Number}", "Удаление узла учета", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No) return;

            var counter = (ConsumptionCounter) countersGrid.SelectedItem;
            var associatedCounters = ContextFactory.Instance.ConsumptionCounters.Where(c => c.AssociatedDeviceID == counter.ID);
            foreach (var associatedCounter in associatedCounters)
            {
                ContextFactory.Instance.ConsumptionCounters.Remove(associatedCounter);
            }
            ContextFactory.Instance.ConsumptionCounters.Remove(counter);
            ContextFactory.Instance.SaveChanges();
            UpdateData();
            App.AppMainWindow.SetUpdateData();
        }
    }
}
