﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AccountingСounters.CustomControls;
using AccountingСounters.Database;
using AccountingСounters.Enums;
using AccountingСounters.Models;

namespace AccountingСounters.Interface.Facility
{
    /// <summary>
    /// Interaction logic for AddAllCountersIndications.xaml
    /// </summary>
    public partial class AddAllCountersIndications : Window
    {
        MaintainedFacility Facility { get; set; }
        public AddAllCountersIndications(MaintainedFacility facility)
        {
            InitializeComponent();
            monthPicker.SelectedDate = DateTime.Now.AddDays(-(DateTime.Now.Day - 1));
            Facility = facility;
            if (!ContextFactory.Instance.Entry(Facility).Collection(f => f.Counters).IsLoaded)
            {
                ContextFactory.Instance.Entry(Facility).Collection(f => f.Counters).Load();
            }
            var ColdWaterCounters = Facility.Counters.Where(c => c.Type == CounterType.ColdWaterNode);
            var HotWaterCounters = Facility.Counters.Where(c => c.Type == CounterType.HotWaterNode);
            var ElectricityCounters = Facility.Counters.Where(c => c.Type == CounterType.ElectricNode);
            var HeatCounters = Facility.Counters.Where(c => c.Type == CounterType.HeatEnergyNode);

            foreach (var counter in ColdWaterCounters)
            {
                contentPanel.Children.Add(new IndicationsInput(counter, monthPicker.SelectedDate.Value));
            }
            foreach (var counter in HotWaterCounters)
            {
                contentPanel.Children.Add(new IndicationsInput(counter, monthPicker.SelectedDate.Value));
            }
            foreach (var counter in ElectricityCounters)
            {
                contentPanel.Children.Add(new IndicationsInput(counter, monthPicker.SelectedDate.Value));
            }
            foreach (var counter in HeatCounters)
            {
                contentPanel.Children.Add(new IndicationsInput(counter, monthPicker.SelectedDate.Value));
            }
        }

        private void monthPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var indicationsInput in contentPanel.Children.OfType<IndicationsInput>())
            {
                indicationsInput.Date = monthPicker.SelectedDate.Value;
                indicationsInput.UpdateLastIndication();
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (contentPanel.Children.OfType<IndicationsInput>().Any(indicationsInput => indicationsInput.Value == ""))
            {
                MessageBox.Show("Заполнены не все поля", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            foreach (var indicationsInput in contentPanel.Children.OfType<IndicationsInput>())
            {
                indicationsInput.SaveCounterIndication();
            }
            //((FacilityCard)Owner).UpdateData();
            Close();
        }
    }
}
