﻿using System.Collections.Generic;
using System.Windows;
using AccountingСounters.Database;
using AccountingСounters.Models;

namespace AccountingСounters.Interface.Facility
{
    /// <summary>
    /// Interaction logic for AddNewFacility.xaml
    /// </summary>
    public partial class AddNewFacility : Window
    {
        private bool add;
        public AddNewFacility()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!add && MessageBox.Show("Все не сохраненные изменения будут утеряны.\n Закрыть?", "Отмена", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                add = false;
                e.Cancel = true;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            if (facilityName.Text == "" ||
                facilityAdress.Text == "" ||

                managerLastName.Text == "" ||
                managerName.Text == "" ||
                managerSurname.Text == "" ||
                managerPhone.Text == "" ||

                liableLastName.Text == "" ||
                liableName.Text == "" ||
                liableSurName.Text == "" ||
                liablePhone.Text == "" ||
                sentryPhone.Text == "")
            {
                MessageBox.Show("Заполнены не все поля", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            add = true;
            ContextFactory.Instance.MaintainedFacilities.Add(new MaintainedFacility()
            {
                Name = facilityName.Text,
                Address = facilityAdress.Text,
                ManagerName = $"{managerLastName.Text} {managerName.Text} {managerSurname.Text}",
                ManagerPhone = managerPhone.Text,
                IndicationsLiableName = $"{liableLastName.Text} {liableName.Text} {liableSurName.Text}",
                IndicationsLiablePhone = liablePhone.Text,
                SentryPhone = sentryPhone.Text,
                Counters = new List<ConsumptionCounter>()
            });

            ContextFactory.Instance.SaveChanges();
            App.AppMainWindow.SetUpdateData();
            Close();
        }
    }
}
