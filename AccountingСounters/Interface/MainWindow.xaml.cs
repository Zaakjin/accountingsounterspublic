﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using AccountingСounters.Database;
using AccountingСounters.Enums;
using AccountingСounters.Interface.Facility;
using AccountingСounters.Models;
using ClosedXML.Excel;
using Data.Enums;

namespace AccountingСounters.Interface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //public static ICollection Data { get; set; }

        public void SetUpdateData()
        {
            //mainDataGrid.ItemsSource =  ContextFactory.Instance.MaintainedFacilities.ToList();
            mainDataGrid.ItemsSource = ContextFactory.Instance.MaintainedFacilities.Include(f => f.Counters).ToList();
        }
        public MainWindow()
        {
            InitializeComponent();
            App.AppMainWindow = this;
            SetUpdateData();
            facilityHeatNodes.Header = CounterType.HeatEnergyNode.GetDisplayName();
            facilityCWNodes.Header = CounterType.ColdWaterNode.GetDisplayName();
            facilityHWNodes.Header = CounterType.HotWaterNode.GetDisplayName();
            facilityElectricityNode.Header = CounterType.ElectricNode.GetDisplayName();

            //Название объекта
            facilityNameColumn.Binding = new Binding(nameof(MaintainedFacility.Name));

            //ФИО и телефон директора
            facilityManager.Binding = new Binding(nameof(MaintainedFacility.GetManagerInfo));

            //Телефон дежурного
            facilitySentryPhone.Binding = new Binding(nameof(MaintainedFacility.SentryPhone));

            //ФИО и телефон ответственного за подачу показаний
            facilityLiable.Binding = new Binding(nameof(MaintainedFacility.GetLiableInfo));

            //Количество приборов учета тепла
            facilityHeatNodes.Binding = new Binding(nameof(MaintainedFacility.GetHeatEnergyNodeCount));
            //Количество приборов учета ГВ
            facilityHWNodes.Binding = new Binding(nameof(MaintainedFacility.GetHotWaterNodeCount));
            //Количество приборов учета ХВ
            facilityCWNodes.Binding = new Binding(nameof(MaintainedFacility.GetColdWaterNodeCount));
            //Количество приборов учета электричества
            facilityElectricityNode.Binding = new Binding(nameof(MaintainedFacility.GetElectricityNodeCount));

            //Кнопка карточка обьекта
            Binding facilityCardBinding = new Binding();
            facilityCardBinding.Converter = new IsDataGridItemSelected();
            facilityCardBinding.Source = this.mainDataGrid;
            facilityCardBinding.Path = new PropertyPath("SelectedIndex");
            //TODO btnFacilityCard.SetBinding(Button.IsEnabledProperty, facilityCardBinding);
            //Кнопка карточка обьекта c картинкой
            btnImgFacilityCard.SetBinding(Button.IsEnabledProperty, facilityCardBinding);
        }

        private void btnAddNewFacility_Click(object sender, RoutedEventArgs e)
        {
            var addNewFacilityWnd = new Facility.AddNewFacility();
            addNewFacilityWnd.ShowDialog();
        }

        private void mainDataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if(mainDataGrid.SelectedItem == null) return;
            var facilityCardWnd = new Facility.FacilityCard();
            facilityCardWnd.Owner = this;
            facilityCardWnd.ShowDialog();
        }

        private void btnFacilityCard_Click(object sender, RoutedEventArgs e)
        {
            if (mainDataGrid.SelectedItem == null) return;
            var facilityCardWnd = new Facility.FacilityCard();
            facilityCardWnd.Owner = this;
            facilityCardWnd.ShowDialog();
        }

        public class IsDataGridItemSelected : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return (int)value > -1;
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return value;
            }
        }

        //private void btnImgIndicationsFromFile_Click(object sender, RoutedEventArgs e)
        //{

        //    Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
        //    //dlg.FileName = $"Форма подачи показаний - {Facility.Name}"; // Default file name
        //    dlg.DefaultExt = ".xlsx";               // Default file extension
        //    dlg.Filter = "Таблица (.xlsx)|*.xlsx";  // Filter files by extension
        //    dlg.AddExtension = true;

        //    // Show save file dialog box
        //    bool? result = dlg.ShowDialog();

        //    // Process save file dialog box results
        //    if (result == true)
        //    {
        //        // Save document
        //        string filename = dlg.FileName;
        //        FileInfo file = new FileInfo(dlg.FileName);
        //        try
        //        {
        //            using (FileStream fileStream = file.Open(FileMode.Open,
        //                FileAccess.ReadWrite, FileShare.ReadWrite))
        //            {
        //                var workbook = new XLWorkbook(fileStream);
        //                var worksheet = workbook.Worksheet("Indications");
        //                var facilityID = int.Parse(worksheet.Cell(1, 1).Value.ToString());
        //                AddAllCountersIndications allCountersIndications = new AddAllCountersIndications(ContextFactory.Instance.MaintainedFacilities.Single(f => f.ID == facilityID));
        //                allCountersIndications.monthPicker.SelectedDate = DateTime.Parse(worksheet.Cell(4, 3).Value.ToString());
        //                Dictionary<int, double> counters = new Dictionary<int, double>();
        //                var row = worksheet.Row(7);
        //                while (!row.Cell(1).IsEmpty())
        //                {
        //                    counters.Add(int.Parse(row.Cell(1).Value.ToString()), double.Parse(row.Cell(4).Value.ToString().Replace('.', ',')));
        //                    row = row.RowBelow();
        //                }
        //                foreach (var indicationsInput in allCountersIndications.contentPanel.Children.OfType<IndicationsInput>())
        //                {
        //                    indicationsInput.currentIndication.Text = counters[indicationsInput.Counter.ID].ToString(CultureInfo.CurrentCulture);
        //                }
        //                allCountersIndications.ShowDialog();
        //            }
        //        }
        //        catch (Exception exception)
        //        {
        //            MessageBox.Show(exception.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Warning);
        //            //Console.WriteLine(exception);
        //            //throw;
        //        }

        //    }
        //}

        private void btnImgIndicationsFromFile_Click(object sender, RoutedEventArgs e)
        {

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".xlsx";               // Default file extension
            dlg.Filter = "Таблица (.xlsx)|*.xlsx";  // Filter files by extension
            dlg.AddExtension = true;
            dlg.Multiselect = true;

            // Show save file dialog box
            bool? result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string[] filenames = dlg.FileNames;

                foreach (string filename in filenames)
                {
                    FileInfo fileInfo = new FileInfo(filename);
                    try
                    {
                        using (FileStream fileStream = fileInfo.Open(FileMode.Open,
                            FileAccess.ReadWrite, FileShare.ReadWrite))
                        {
                            var workbook = new XLWorkbook(fileStream);
                            var worksheet = workbook.Worksheet("Indications");
                            var facilityID = int.Parse(worksheet.Cell(1, 1).Value.ToString());
                            DateTime date = DateTime.Parse(worksheet.Cell(4, 3).Value.ToString());
                            Dictionary<int, double> counters = new Dictionary<int, double>();
                            var row = worksheet.Row(7);
                            while (!row.Cell(1).IsEmpty())
                            {
                                if (row.Cell(1).Value.ToString() != FacilityCard.EXCEL_FILLER)
                                {
                                    counters.Add(int.Parse(row.Cell(1).Value.ToString()), double.Parse(row.Cell(4).Value.ToString().Replace('.', ',')));
                                }
                                row = row.RowBelow();
                            }
                            foreach (KeyValuePair<int, double> counter in counters)
                            {
                                var Counter = ContextFactory.Instance.ConsumptionCounters.Single(c => c.ID == counter.Key);
                                var tempIndication = ContextFactory.Instance.Indications.SingleOrDefault(i => i.ConsumptionCounterID == Counter.ID &&
                                                                                                              i.Date.Year == date.Year &&
                                                                                                              i.Date.Month == date.Month);
                                if (tempIndication != null)
                                {
                                    tempIndication.Value = counter.Value;
                                }
                                else
                                {
                                    ContextFactory.Instance.Indications.Add(new Indication(counter.Value, Counter, date));
                                }
                            }
                            ContextFactory.Instance.SaveChanges();
                        }
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show($"Ошибка в файле {fileInfo.Name}\n\n" + exception.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
                MessageBox.Show("Показания добавлены", "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnImgOpenDatabase_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".sqlite";               // Default file extension
            dlg.Filter = "База данных (.sqlite)|*.sqlite";  // Filter files by extension
            dlg.AddExtension = true;

            // Show save file dialog box
            bool? result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                MessageBox.Show(dlg.FileName);
                string filename = dlg.FileName;
                FileInfo file = new FileInfo(dlg.FileName);

            }
            else
            {
                return;
            }
            if (dlg.FileName.IndexOf("\\") == 0) dlg.FileName = "\\" + dlg.FileName;
            var connectionString = new SQLiteConnectionStringBuilder("data source =" + dlg.FileName).ConnectionString;
            var connection = new SQLiteConnection(connectionString);
            connection.Open();
            ContextFactory.Reset();
            ContextFactory.Instance = new DataModel(connection);
            SetUpdateData();
        }

        private void btnImgCreateReport_Click(object sender, RoutedEventArgs e)
        {
            var createReportWindow = new CreateReport();
            createReportWindow.ShowDialog();
        }
    }
}
